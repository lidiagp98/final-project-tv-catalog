package com.applaudotrainee.tvcatalog.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.applaudotrainee.tvcatalog.data.util.Connectivity
import com.applaudotrainee.tvcatalog.domain.session.interaction.LogoutUseCase
import kotlinx.coroutines.launch
import org.koin.java.KoinJavaComponent

class MainViewModel(private val logoutUseCase: LogoutUseCase) : ViewModel() {

    val connectivity: Connectivity by KoinJavaComponent.inject(Connectivity::class.java)

    fun deleteSessionId() = viewModelScope.launch {
        logoutUseCase()
    }
}