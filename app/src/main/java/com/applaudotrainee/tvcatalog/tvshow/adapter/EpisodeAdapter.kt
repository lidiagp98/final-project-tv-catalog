package com.applaudotrainee.tvcatalog.tvshow.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.applaudotrainee.tvcatalog.databinding.CastItemBinding
import com.applaudotrainee.tvcatalog.databinding.EpisodeItemBinding
import com.applaudotrainee.tvcatalog.domain.tvshow.model.Cast
import com.applaudotrainee.tvcatalog.domain.tvshow.model.Episode

class EpisodeAdapter() :
    ListAdapter<Episode, EpisodeAdapter.EpisodeViewHolder>(EpisodeDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EpisodeViewHolder {
        return EpisodeViewHolder(EpisodeItemBinding.inflate(LayoutInflater.from(parent.context)))
    }

    override fun onBindViewHolder(holder: EpisodeViewHolder, position: Int) {
        val episode = getItem(position)
        holder.bind(episode)
    }

    class EpisodeViewHolder(private var binding: EpisodeItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(episode: Episode) {
            binding.episode = episode
            binding.executePendingBindings()
        }

    }

    class EpisodeDiffCallback : DiffUtil.ItemCallback<Episode>() {
        override fun areItemsTheSame(oldItem: Episode, newItem: Episode): Boolean {
            return oldItem === newItem
        }

        override fun areContentsTheSame(oldItem: Episode, newItem: Episode): Boolean {
            return (oldItem.episodeNumber == newItem.episodeNumber &&
                    oldItem.name == newItem.name &&
                    oldItem.stillPath == newItem.stillPath &&
                    oldItem.id == newItem.id &&
                    oldItem.overview == newItem.overview &&
                    oldItem.voteAverage == newItem.voteAverage &&
                    oldItem.seasonNumber == newItem.seasonNumber
                    )
        }

    }
}