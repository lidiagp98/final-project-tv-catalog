package com.applaudotrainee.tvcatalog.tvshow.viewmodel

import androidx.lifecycle.*
import androidx.paging.PagedList
import com.applaudotrainee.tvcatalog.data.util.Connectivity
import com.applaudotrainee.tvcatalog.domain.tvshow.interaction.GetTvShowListUseCase
import com.applaudotrainee.tvcatalog.domain.tvshow.model.TvShow
import com.applaudotrainee.tvcatalog.domain.util.ShowFilter
import kotlinx.coroutines.launch
import org.koin.java.KoinJavaComponent

class TvShowViewModel(
    private val getTvShowListUseCase: GetTvShowListUseCase<LiveData<PagedList<TvShow>>>
) :
    ViewModel() {

    private val connectivity: Connectivity by KoinJavaComponent.inject(Connectivity::class.java)

    val isConnected = connectivity.isNetworkConnected

    private lateinit var _tvShowList: LiveData<PagedList<TvShow>>
    val tvShowList = MutableLiveData<PagedList<TvShow>>()

    val mediatorTvShowList: MediatorLiveData<Unit> = MediatorLiveData()

    fun getListOfShowsFilter(filter: ShowFilter) {
        viewModelScope.launch {
            _tvShowList = getTvShowListUseCase(filter)
            mediatorTvShowList.addSource(_tvShowList) {
                tvShowList.value = it
            }
        }
    }
}

