package com.applaudotrainee.tvcatalog.tvshow.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import com.applaudotrainee.tvcatalog.common.FlowStatus
import com.applaudotrainee.tvcatalog.domain.tvshow.interaction.GetEpisodesUseCase
import com.applaudotrainee.tvcatalog.domain.tvshow.model.Season

class SeasonViewModel(
    getEpisodesUseCase: GetEpisodesUseCase,
    showId: Int
) : ViewModel() {
    val flowStatus = MutableLiveData(FlowStatus.LOADING)
    val seasons = getEpisodesUseCase(showId).asLiveData()
}