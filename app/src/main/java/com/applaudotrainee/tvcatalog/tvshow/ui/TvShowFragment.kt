package com.applaudotrainee.tvcatalog.tvshow.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.appcompat.app.AlertDialog
import androidx.core.view.get
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import androidx.recyclerview.widget.GridLayoutManager
import com.applaudotrainee.tvcatalog.R
import com.applaudotrainee.tvcatalog.common.buildDialog
import com.applaudotrainee.tvcatalog.common.gone
import com.applaudotrainee.tvcatalog.common.visible
import com.applaudotrainee.tvcatalog.databinding.FragmentTvshowBinding
import com.applaudotrainee.tvcatalog.domain.util.ShowFilter
import com.applaudotrainee.tvcatalog.tvshow.viewmodel.TvShowViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class TvShowFragment : Fragment() {

    private val tvShowViewModel: TvShowViewModel by viewModel()
    private lateinit var binding: FragmentTvshowBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_tvshow, container, false)
        binding.lifecycleOwner = this

        //Binding for Recycler
        binding.tvShowRecycler.layoutManager = GridLayoutManager(context, SPAN_COUNT)

        val showListAdapter = TvShowPagingAdapter(TvShowPagingAdapter.ShowListener { showId ->
            val action = TvShowFragmentDirections.actionTvShowFragmentToShowDetailsFragment(showId)
            view?.let { Navigation.findNavController(it).navigate(action) }
        })
        binding.tvShowRecycler.adapter = showListAdapter

        tvShowViewModel.mediatorTvShowList.observe(viewLifecycleOwner, {})

        tvShowViewModel.tvShowList.observe(viewLifecycleOwner, Observer { tvShowList ->
            binding.refresh.isRefreshing = false
            showListAdapter.submitList(tvShowList)
        })

        //Binding for spinner
        val filters = resources.getStringArray(R.array.Filters)
        val spinnerAdapter =
            context?.let {
                ArrayAdapter(
                    it,
                    android.R.layout.simple_spinner_dropdown_item,
                    filters
                )
            }
        binding.filterMenu.adapter = spinnerAdapter
        spinnerListener()

        tvShowViewModel.isConnected.observe(viewLifecycleOwner, Observer {
            if (it) binding.noWifiTxt.gone() else binding.noWifiTxt.visible()
        })

        //Refresh
        binding.refresh.setOnRefreshListener {
            if (tvShowViewModel.isConnected.value == true){
                selectFilter(binding.filterMenu.selectedItemPosition)
            } else {
                binding.refresh.isRefreshing = false
                context.buildDialog()
            }
        }

        return binding.root
    }

    private fun spinnerListener() {
        binding.filterMenu.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected( parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                selectFilter(position)
                if (tvShowViewModel.isConnected.value != true) {
                    context.buildDialog()
                }
            }
            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }
    }

    private fun selectFilter(position: Int){
        when (position) {
            0 -> tvShowViewModel.getListOfShowsFilter(ShowFilter.POPULAR)
            1 -> tvShowViewModel.getListOfShowsFilter(ShowFilter.TOP_RATED)
            2 -> tvShowViewModel.getListOfShowsFilter(ShowFilter.ON_TV)
            3 -> tvShowViewModel.getListOfShowsFilter(ShowFilter.AIRING_TODAY)
        }
    }

    companion object {
        private const val SPAN_COUNT = 2
    }
}