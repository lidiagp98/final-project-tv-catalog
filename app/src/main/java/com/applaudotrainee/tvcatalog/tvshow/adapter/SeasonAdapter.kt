package com.applaudotrainee.tvcatalog.tvshow.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.applaudotrainee.tvcatalog.databinding.SeasonItemBinding
import com.applaudotrainee.tvcatalog.domain.tvshow.model.SeasonWithEpisode

class SeasonAdapter() :
    ListAdapter<SeasonWithEpisode, SeasonAdapter.SeasonViewHolder>(SeasonDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SeasonViewHolder {
        return SeasonViewHolder(SeasonItemBinding.inflate(LayoutInflater.from(parent.context)))
    }

    override fun onBindViewHolder(holder: SeasonViewHolder, position: Int) {
        val seasonWithEpisode = getItem(position)
        holder.bind(seasonWithEpisode)
    }

    class SeasonViewHolder(private var binding: SeasonItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(seasonWithEpisode: SeasonWithEpisode) {
            binding.seasonWithEpisode = seasonWithEpisode
            val adapter = EpisodeAdapter()
            binding.episodesRecycler.adapter = adapter
            adapter.submitList(seasonWithEpisode.episodeList)
            binding.executePendingBindings()
        }
    }

    class SeasonDiffCallback : DiffUtil.ItemCallback<SeasonWithEpisode>() {
        override fun areItemsTheSame(
            oldItem: SeasonWithEpisode,
            newItem: SeasonWithEpisode
        ): Boolean {
            return oldItem === newItem
        }

        override fun areContentsTheSame(
            oldItem: SeasonWithEpisode,
            newItem: SeasonWithEpisode
        ): Boolean {
            return (oldItem.season.seasonNumber == newItem.season.seasonNumber &&
                    oldItem.season.name == newItem.season.name &&
                    oldItem.season.posterPath == newItem.season.posterPath &&
                    oldItem.season.seasonId == newItem.season.seasonId &&
                    oldItem.episodeList.toTypedArray() contentEquals newItem.episodeList.toTypedArray())
        }
    }
}