package com.applaudotrainee.tvcatalog.tvshow.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.applaudotrainee.tvcatalog.R
import com.applaudotrainee.tvcatalog.common.FlowStatus
import com.applaudotrainee.tvcatalog.common.gone
import com.applaudotrainee.tvcatalog.common.visible
import com.applaudotrainee.tvcatalog.databinding.FragmentSeasonsBinding
import com.applaudotrainee.tvcatalog.tvshow.adapter.SeasonAdapter
import com.applaudotrainee.tvcatalog.tvshow.viewmodel.SeasonViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf


class SeasonsFragment : Fragment() {

    private val args: SeasonsFragmentArgs by navArgs()
    private val seasonViewModel: SeasonViewModel by viewModel { parametersOf(args.tvShowId) }
    private lateinit var binding: FragmentSeasonsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_seasons, container, false)
        binding.lifecycleOwner = this

        val seasonAdapter = SeasonAdapter()
        binding.seasonRecycler.adapter = seasonAdapter

        seasonViewModel.seasons.observe(viewLifecycleOwner, Observer {
            seasonViewModel.flowStatus.value = FlowStatus.SUCCESS
            seasonAdapter.submitList(it)
        })

        seasonViewModel.flowStatus.observe(viewLifecycleOwner, Observer {
            when(it){
                FlowStatus.SUCCESS -> binding.progressEpisode.gone()
                FlowStatus.LOADING -> binding.progressEpisode.visible()
            }
        })
        return binding.root
    }
}