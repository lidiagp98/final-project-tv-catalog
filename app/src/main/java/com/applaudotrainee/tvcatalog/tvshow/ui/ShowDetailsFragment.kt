package com.applaudotrainee.tvcatalog.tvshow.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.applaudotrainee.tvcatalog.R
import com.applaudotrainee.tvcatalog.common.buildDialog
import com.applaudotrainee.tvcatalog.common.visible
import com.applaudotrainee.tvcatalog.databinding.FragmentShowDetailsBinding
import com.applaudotrainee.tvcatalog.tvshow.adapter.CastAdapter
import com.applaudotrainee.tvcatalog.tvshow.viewmodel.ShowDetailsViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class ShowDetailsFragment : Fragment() {

    private lateinit var binding: FragmentShowDetailsBinding
    private val args: ShowDetailsFragmentArgs by navArgs()
    private val showDetailsViewModel: ShowDetailsViewModel by viewModel { parametersOf(args.showId) }

    companion object {
        private const val ADD_CODE = 1
        private const val REMOVE_CODE = 13
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_show_details, container, false)
        binding.showViewModel = showDetailsViewModel
        binding.lifecycleOwner = this

        binding.castRecycler.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        val castAdapter = CastAdapter()
        binding.castRecycler.adapter = castAdapter

        showDetailsViewModel.showDetails.observe(viewLifecycleOwner, Observer { showWithAddon ->
            castAdapter.submitList(showWithAddon.castList)
        })

        binding.allSeasonsBtn.setOnClickListener {
            if (showDetailsViewModel.isConnected.value == true) {
                binding.progressBar2.visible()
                val action =
                    ShowDetailsFragmentDirections.actionShowDetailsFragmentToSeasonsFragment(args.showId)
                view?.let { Navigation.findNavController(it).navigate(action) }
            } else context.buildDialog()
        }

        binding.favoriteBtn.setOnClickListener {
            showDetailsViewModel.addRemoveFavorite()
        }

        showDetailsViewModel.statusMessage.observe(viewLifecycleOwner, Observer {
            if (it.statusCode == ADD_CODE || it.statusCode == REMOVE_CODE) {
                showDetailsViewModel.isFavorite.value =
                    showDetailsViewModel.isFavorite.value != true
            }
        })

        return binding.root
    }
}