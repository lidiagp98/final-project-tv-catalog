package com.applaudotrainee.tvcatalog.tvshow.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.applaudotrainee.tvcatalog.databinding.ShowItemBinding
import com.applaudotrainee.tvcatalog.domain.tvshow.model.TvShow
import com.applaudotrainee.tvcatalog.tvshow.ui.TvShowPagingAdapter.ShowViewHolder

class TvShowPagingAdapter(private val clickListener: ShowListener) :
    PagedListAdapter<TvShow, ShowViewHolder>(TvShowDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ShowViewHolder {
        return ShowViewHolder(ShowItemBinding.inflate(LayoutInflater.from(parent.context)))
    }

    override fun onBindViewHolder(holder: ShowViewHolder, position: Int) {
        getItem(position)?.let { tvShow -> holder.bind(tvShow, clickListener) }
    }

    class ShowViewHolder(private var binding: ShowItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(tvShow: TvShow, clickListener: ShowListener) {
            binding.show = tvShow
            binding.clickListener = clickListener
            binding.executePendingBindings()
        }

    }

    class TvShowDiffCallback : DiffUtil.ItemCallback<TvShow>() {
        override fun areItemsTheSame(oldItem: TvShow, newItem: TvShow): Boolean {
            return oldItem == newItem
        }

        override fun areContentsTheSame(oldItem: TvShow, newItem: TvShow): Boolean {
            return (oldItem.airDate == newItem.airDate &&
                    oldItem.backdropUrl == newItem.backdropUrl &&
                    oldItem.description == newItem.description &&
                    oldItem.posterUrl == newItem.posterUrl &&
                    oldItem.score == newItem.score &&
                    oldItem.showId == newItem.showId &&
                    oldItem.showName == newItem.showName &&
                    oldItem.order == oldItem.order
                    )
        }

    }

    class ShowListener(val clickListener: (showId: Int) -> Unit) {
        fun onClick(show: TvShow) = clickListener(show.showId)
    }
}