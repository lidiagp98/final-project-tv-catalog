package com.applaudotrainee.tvcatalog.tvshow.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.applaudotrainee.tvcatalog.data.util.Connectivity
import com.applaudotrainee.tvcatalog.domain.tvshow.interaction.AddRemoveFavoriteUseCase
import com.applaudotrainee.tvcatalog.domain.tvshow.interaction.GetAccountStateUseCase
import com.applaudotrainee.tvcatalog.domain.tvshow.interaction.GetShowDetailsUseCase
import com.applaudotrainee.tvcatalog.domain.tvshow.model.FavoriteResponse
import com.applaudotrainee.tvcatalog.domain.tvshow.model.ShowWithAddon
import kotlinx.coroutines.launch
import org.koin.java.KoinJavaComponent

class ShowDetailsViewModel(
    getShowDetailsUseCase: GetShowDetailsUseCase,
    private val showId: Int,
    private val addRemoveFavoriteUseCase: AddRemoveFavoriteUseCase,
    private val getAccountStateUseCase: GetAccountStateUseCase
) : ViewModel() {

    private val connectivity: Connectivity by KoinJavaComponent.inject(Connectivity::class.java)
    val isConnected = connectivity.isNetworkConnected

    val showDetails = MutableLiveData<ShowWithAddon>()
    val statusMessage = MutableLiveData<FavoriteResponse>()
    val isFavorite = MutableLiveData<Boolean>()

    fun addRemoveFavorite() {
        viewModelScope.launch {
            if (isConnected.value == true) {
                statusMessage.postValue(addRemoveFavoriteUseCase(showId))
            }
        }
    }

    init {
        viewModelScope.launch {
            if (isConnected.value == true) {
                isFavorite.postValue(getAccountStateUseCase(showId)?.favorite)
            }
            showDetails.postValue(getShowDetailsUseCase(showId))
        }
    }
}