package com.applaudotrainee.tvcatalog

import android.app.Application
import com.applaudotrainee.tvcatalog.data.di.databaseModule
import com.applaudotrainee.tvcatalog.data.di.networkModule
import com.applaudotrainee.tvcatalog.data.di.repositoryModule
import com.applaudotrainee.tvcatalog.di.presentationModule
import com.applaudotrainee.tvcatalog.domain.di.interactionModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class CatalogApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@CatalogApplication)
            modules(appModules + domainModules + dataModules)
        }
    }
}

val appModules = listOf(presentationModule)
val domainModules = listOf(interactionModule)
val dataModules = listOf(networkModule, repositoryModule, databaseModule)