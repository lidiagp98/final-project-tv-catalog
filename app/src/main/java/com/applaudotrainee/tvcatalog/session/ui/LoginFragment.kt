package com.applaudotrainee.tvcatalog.session.ui

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.*
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import com.applaudotrainee.tvcatalog.R
import com.applaudotrainee.tvcatalog.common.FlowStatus
import com.applaudotrainee.tvcatalog.common.gone
import com.applaudotrainee.tvcatalog.common.visible
import com.applaudotrainee.tvcatalog.databinding.FragmentLoginBinding
import com.applaudotrainee.tvcatalog.domain.session.model.TokenResponse
import com.applaudotrainee.tvcatalog.session.viewmodel.LoginViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel

class LoginFragment : Fragment() {

    companion object {
        const val AUTHENTICATE_URL = "https://www.themoviedb.org/authenticate/"
    }

    private val loginViewModel: LoginViewModel by viewModel()
    private lateinit var binding: FragmentLoginBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_login, container, false)

        binding.lifecycleOwner = this
        binding.loginViewModel = loginViewModel

        binding.webView.gone()

        binding.loginBtn.setOnClickListener {
            if (loginViewModel.sessionId.value == null) {
                CoroutineScope(Dispatchers.IO).launch {
                    loginViewModel.token.postValue(getRequestToken())
                }
            } else {
                Navigation.findNavController(binding.root)
                    .navigate(R.id.action_loginFragment_to_tvShowFragment)
            }
        }

        loginViewModel.token.observe(viewLifecycleOwner, Observer { tokenResponse ->
            if (loginViewModel.flowStatus.value == FlowStatus.SUCCESS) {
                binding.webView.visible()
                binding.loginBtn.gone()
                setWebView(tokenResponse)
            }
        })

        loginViewModel.sessionId.observe(viewLifecycleOwner, Observer { sessionResponse ->
            if (sessionResponse != null) {
                Navigation.findNavController(binding.root)
                    .navigate(R.id.action_loginFragment_to_tvShowFragment)
            }
        })

        loginViewModel.flowStatus.observe(viewLifecycleOwner, Observer {
            when (it) {
                FlowStatus.NO_INTERNET -> {
                    binding.webView.gone()
                    binding.loginBtn.visible()
                    binding.loginBtn.text = getString(R.string.try_again)
                    binding.noInternetImg.visible()
                    binding.icon.gone()
                    Toast.makeText(context, getString(R.string.no_connection), Toast.LENGTH_SHORT)
                        .show()
                }
                FlowStatus.SUCCESS -> {
                    binding.webView.visible()
                    binding.loginBtn.visible()
                    binding.icon.visible()
                    binding.noInternetImg.gone()
                }
            }
        })

        return binding.root
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        val profileItem: MenuItem = menu.findItem(R.id.loginFragment)
        val loginItem: MenuItem = menu.findItem(R.id.profileFragment)
        profileItem.isVisible = false
        loginItem.isVisible = false
    }

    private suspend fun getRequestToken() = loginViewModel.getRequestToken()

    private suspend fun getSessionId(accessToken: String) = loginViewModel.getSessionId(accessToken)

    @SuppressLint("SetJavaScriptEnabled")
    private fun setWebView(tokenResponse: TokenResponse) {
        binding.webView.loadUrl(AUTHENTICATE_URL + tokenResponse.requestToken)
        binding.webView.settings.javaScriptEnabled = true
        binding.webView.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
                url?.let {
                    view?.loadUrl(url)
                }
                return false
            }

            override fun onPageFinished(view: WebView?, url: String?) {
                super.onPageFinished(view, url)
                url?.let {
                    if (url.contains("/allow")) {
                        CoroutineScope(Dispatchers.IO).launch {
                            loginViewModel.sessionId.postValue(getSessionId(tokenResponse.requestToken))
                        }
                    }
                }
            }
        }
    }
}