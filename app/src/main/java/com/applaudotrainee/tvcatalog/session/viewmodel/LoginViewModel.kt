package com.applaudotrainee.tvcatalog.session.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.applaudotrainee.tvcatalog.common.FlowStatus
import com.applaudotrainee.tvcatalog.data.util.Connectivity
import com.applaudotrainee.tvcatalog.domain.session.interaction.GetRequestToken
import com.applaudotrainee.tvcatalog.domain.session.interaction.GetSessionIdUseCase
import com.applaudotrainee.tvcatalog.domain.session.interaction.GetSessionIdFromDbUseCase
import com.applaudotrainee.tvcatalog.domain.session.model.SessionResponse
import com.applaudotrainee.tvcatalog.domain.session.model.TokenResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.koin.java.KoinJavaComponent.inject

class LoginViewModel(
    private val getTokenUseCase: GetRequestToken,
    private val getSessionIdUseCase: GetSessionIdUseCase,
    private val getSessionIdFromDbUseCase: GetSessionIdFromDbUseCase
) : ViewModel() {

    private val connectivity: Connectivity by inject(Connectivity::class.java)

    val flowStatus = MutableLiveData<FlowStatus>()
    private val isConnected = connectivity.isNetworkConnected

    val token = MutableLiveData<TokenResponse>()
    val sessionId = MutableLiveData<SessionResponse>()

    suspend fun getRequestToken(): TokenResponse? {
        flowStatus.postValue(FlowStatus.LOADING)
        return if (isConnected.value == true) {
            val response = getTokenUseCase()
            flowStatus.postValue(if (response == null) FlowStatus.ERROR else FlowStatus.SUCCESS)
            response
        } else {
            flowStatus.postValue(FlowStatus.NO_INTERNET)
            null
        }
    }

    suspend fun getSessionId(accessToken: String): SessionResponse? {
        flowStatus.postValue(FlowStatus.LOADING)
        return if (isConnected.value == true) {
            val response = getSessionIdUseCase(accessToken)
            flowStatus.postValue(if (response == null) FlowStatus.ERROR else FlowStatus.SUCCESS)
            response
        } else {
            null
        }
    }

    private suspend fun getSessionIdFromDb() {
        flowStatus.postValue(FlowStatus.LOADING)
        withContext(Dispatchers.IO) {
            val response = getSessionIdFromDbUseCase()
            flowStatus.postValue(if (response?.success == true) FlowStatus.SUCCESS else FlowStatus.ERROR)
            sessionId.postValue(response)
        }
    }

    init {
        viewModelScope.launch { getSessionIdFromDb() }
    }

}