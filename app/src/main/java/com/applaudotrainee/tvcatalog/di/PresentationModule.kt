package com.applaudotrainee.tvcatalog.di

import com.applaudotrainee.tvcatalog.session.viewmodel.LoginViewModel
import com.applaudotrainee.tvcatalog.tvshow.viewmodel.SeasonViewModel
import com.applaudotrainee.tvcatalog.tvshow.viewmodel.ShowDetailsViewModel
import com.applaudotrainee.tvcatalog.tvshow.viewmodel.TvShowViewModel
import com.applaudotrainee.tvcatalog.ui.MainViewModel
import com.applaudotrainee.tvcatalog.user.viewmodel.ProfileViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val presentationModule = module {
    viewModel {
        LoginViewModel(
            getTokenUseCase = get(),
            getSessionIdUseCase = get(),
            getSessionIdFromDbUseCase = get()
        )
    }

    viewModel { ProfileViewModel(getUserProfileUseCase = get(), getFavoriteShowsUseCase = get()) }
    viewModel { TvShowViewModel(getTvShowListUseCase = get()) }
    viewModel { (showId: Int) ->
        ShowDetailsViewModel(
            getShowDetailsUseCase = get(),
            showId,
            addRemoveFavoriteUseCase = get(),
            getAccountStateUseCase = get()
        )
    }
    viewModel { (showId: Int) ->
        SeasonViewModel(getEpisodesUseCase = get(), showId)
    }
    viewModel { MainViewModel(logoutUseCase = get()) }
}