package com.applaudotrainee.tvcatalog.common

enum class FlowStatus {
    LOADING, SUCCESS, NO_INTERNET, ERROR
}