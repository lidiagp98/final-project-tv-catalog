package com.applaudotrainee.tvcatalog.common

import android.content.Context
import android.view.View
import androidx.appcompat.app.AlertDialog
import com.applaudotrainee.tvcatalog.R

fun View.visible() {
    visibility = View.VISIBLE
}

fun View.gone() {
    visibility = View.GONE
}

fun Context?.buildDialog() {
    this?.let {
        AlertDialog.Builder(it)
            .setTitle(getString(R.string.no_connection))
            .setMessage(R.string.internet_warning)
            .setPositiveButton(getString(R.string.ok)) { view, _ ->
                view.dismiss()
            }
            .setCancelable(false)
            .create()
    }?.show()
}