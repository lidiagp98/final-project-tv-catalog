package com.applaudotrainee.tvcatalog.common

import android.view.View
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.core.content.ContextCompat.getDrawable
import androidx.core.widget.NestedScrollView
import androidx.databinding.BindingAdapter
import com.applaudotrainee.tvcatalog.R
import com.applaudotrainee.tvcatalog.domain.tvshow.model.Cast
import com.applaudotrainee.tvcatalog.domain.tvshow.model.Creator
import com.applaudotrainee.tvcatalog.domain.tvshow.model.Season
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext


private const val IMAGES_BASE_URL = "https://image.tmdb.org/t/p/original"

@BindingAdapter("loadImage")
fun loadImage(showImg: ImageView, imageUrl: String?) {
    imageUrl?.let {
        val fullImgUrl = IMAGES_BASE_URL + imageUrl
        Glide.with(showImg.context)
            .load(fullImgUrl)
            .placeholder(R.drawable.loading_animation)
            .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
            .error(R.drawable.ic_broken_image)
            .into(showImg)
    }
}

@BindingAdapter("setEmoji")
fun setEmoji(scoreImg: ImageView, score: Double) {
    when (score) {
        in 7.5..10.0 -> scoreImg.setImageResource(R.drawable.ic_excited)
        in 5.5..7.6 -> scoreImg.setImageResource(R.drawable.ic_neutral)
        in 0.0..5.4 -> scoreImg.setImageResource(R.drawable.ic_tired)
        else -> scoreImg.setImageResource(R.drawable.ic_excited)
    }
}

@BindingAdapter("lastSeason")
fun TextView.setLastSeasonName(seasons: List<Season>?) {
    seasons?.let {
        if (seasons.isNotEmpty()){
            val lastSeason = seasons.last()
            text = lastSeason.name
        }
    } ?: run {
        text = context.getString(R.string.info_not_found)
    }
}

@BindingAdapter("creatorName")
fun TextView.setCreatorsName(creators: List<Creator>?) {
    creators?.let {
        val names = creators.map { it.name }
        text = context.getString(R.string.created_by, names.joinToString(", "))
    } ?: run {
        text = context.getString(R.string.info_not_found)
    }
}

@BindingAdapter("setUserId")
fun TextView.setUserId(id: Int) {
    text = context.getString(R.string.user_id, id)
}

@BindingAdapter("seasonImg")
fun ImageView.setSeasonImg(seasons: List<Season>?) {
    seasons?.let {
        if (seasons.isNotEmpty()) {
            val lastSeasonImg = IMAGES_BASE_URL + seasons.last().posterPath
            Glide.with(context)
                .load(lastSeasonImg)
                .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                .placeholder(R.drawable.loading_animation)
                .error(R.drawable.ic_broken_image)
                .into(this)
        }
    }
}

@BindingAdapter("castImg")
fun ImageView.setCastImg(cast: Cast?) {
    cast?.let {
        cast.profilePath?.let {
            val castImg = IMAGES_BASE_URL + cast.profilePath
            Glide.with(context)
                .load(castImg)
                .circleCrop()
                .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                .placeholder(R.drawable.loading_animation)
                .error(R.drawable.ic_broken_image)
                .into(this)
        }
    }
}

@BindingAdapter("isFavorite")
fun ImageView.setFavoriteImage(isFavorite: Boolean) {
    if (isFavorite) setImageResource(R.drawable.ic_bookmark) else setImageResource(R.drawable.ic_bookmarkdisable)
}

@BindingAdapter("loadProgress")
fun ProgressBar.showHideLoading(flowStatus: FlowStatus?) {
    visibility = when (flowStatus) {
        FlowStatus.LOADING -> View.VISIBLE
        else -> View.GONE
    }
}