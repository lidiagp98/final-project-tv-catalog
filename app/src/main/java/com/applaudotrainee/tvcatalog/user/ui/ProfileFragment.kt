package com.applaudotrainee.tvcatalog.user.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import com.applaudotrainee.tvcatalog.R
import com.applaudotrainee.tvcatalog.databinding.FragmentProfileBinding
import com.applaudotrainee.tvcatalog.tvshow.ui.TvShowPagingAdapter
import com.applaudotrainee.tvcatalog.user.viewmodel.ProfileViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel


class ProfileFragment : Fragment() {

    private val profileViewModel: ProfileViewModel by viewModel()
    private lateinit var binding: FragmentProfileBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_profile, container, false)
        binding.lifecycleOwner = this

        val showAdapter = TvShowPagingAdapter(TvShowPagingAdapter.ShowListener { showId ->
            val action =
                ProfileFragmentDirections.actionProfileFragmentToShowDetailsFragment(showId)
            view?.let { Navigation.findNavController(it).navigate(action) }
        })

        binding.favoritesRecycler.adapter = showAdapter

        profileViewModel.mediatorFavoritesList.observe(viewLifecycleOwner, {})

        profileViewModel.user.observe(viewLifecycleOwner, Observer { user ->
            binding.userName.text = user.username
            binding.user = user
        })

        profileViewModel.favoriteList.observe(viewLifecycleOwner, Observer {
            showAdapter.submitList(it)
        })

        return binding.root
    }
}