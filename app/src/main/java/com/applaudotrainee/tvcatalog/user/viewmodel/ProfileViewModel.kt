package com.applaudotrainee.tvcatalog.user.viewmodel

import androidx.lifecycle.*
import androidx.paging.PagedList
import com.applaudotrainee.tvcatalog.domain.tvshow.model.TvShow
import com.applaudotrainee.tvcatalog.domain.user.interaction.GetFavoriteShowsUseCase
import com.applaudotrainee.tvcatalog.domain.user.interaction.GetUserProfileUseCase
import com.applaudotrainee.tvcatalog.domain.user.model.User
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class ProfileViewModel(
    private val getUserProfileUseCase: GetUserProfileUseCase,
    private val getFavoriteShowsUseCase: GetFavoriteShowsUseCase<LiveData<PagedList<TvShow>>>
) : ViewModel() {

    val user = MutableLiveData<User>()
    private lateinit var _favoriteList: LiveData<PagedList<TvShow>>
    val favoriteList = MutableLiveData<PagedList<TvShow>>()

    val mediatorFavoritesList: MediatorLiveData<Unit> = MediatorLiveData()

    private suspend fun getUserProfile() = withContext(Dispatchers.IO) {
        user.postValue(getUserProfileUseCase())
    }

    init {
        viewModelScope.launch {
            getUserProfile()
            _favoriteList = getFavoriteShowsUseCase()
            mediatorFavoritesList.addSource(_favoriteList) {
                favoriteList.value = it
            }
        }
    }
}