package com.applaudotrainee.tvcatalog.domain.session

import com.applaudotrainee.tvcatalog.domain.session.interaction.GetSessionIdUseCaseImpl
import com.applaudotrainee.tvcatalog.domain.session.repository.SessionRepository
import com.applaudotrainee.tvcatalog.domain.sessionResponse
import com.applaudotrainee.tvcatalog.domain.tokenResponse
import kotlinx.coroutines.runBlocking
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito

class GetSessionIdUseCaseImplTest() {
    private val sessionRepositoryMock = Mockito.mock(SessionRepository::class.java)
    private val getSessionIdUseCase = GetSessionIdUseCaseImpl(sessionRepositoryMock)

    @Before
    fun mockFunctionality() {
        runBlocking {
            Mockito.`when`(sessionRepositoryMock.getSessionId(tokenResponse.requestToken)).thenReturn(
                sessionResponse)
        }

    }

    @Test
    fun getSessionId_successfully_thenReturnSessionResponse() {
        runBlocking {
            val response = getSessionIdUseCase(tokenResponse.requestToken)
            assertEquals(sessionResponse, response)
        }
    }
}