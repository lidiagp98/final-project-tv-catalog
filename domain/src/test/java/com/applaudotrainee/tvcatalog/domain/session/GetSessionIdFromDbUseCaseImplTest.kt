package com.applaudotrainee.tvcatalog.domain.session

import com.applaudotrainee.tvcatalog.domain.session.interaction.GetRequestTokenImpl
import com.applaudotrainee.tvcatalog.domain.session.repository.SessionRepository
import com.applaudotrainee.tvcatalog.domain.tokenResponse
import kotlinx.coroutines.runBlocking
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito

class GetSessionIdFromDbUseCaseImplTest() {
    private val sessionRepositoryMock = Mockito.mock(SessionRepository::class.java)
    private val getRequestToken = GetRequestTokenImpl(sessionRepositoryMock)

    @Before
    fun mockFunctionality() {
        runBlocking {
            Mockito.`when`(sessionRepositoryMock.getRequestToken()).thenReturn(tokenResponse)
        }

    }

    @Test
    fun getRequestToken_successfully_thenReturnTokenResponse() {
        runBlocking {
            val response = getRequestToken()
            assertEquals(tokenResponse, response)
        }
    }
}