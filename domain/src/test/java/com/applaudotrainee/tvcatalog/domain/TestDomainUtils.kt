package com.applaudotrainee.tvcatalog.domain

import com.applaudotrainee.tvcatalog.domain.session.model.SessionResponse
import com.applaudotrainee.tvcatalog.domain.session.model.TokenResponse
import com.applaudotrainee.tvcatalog.domain.tvshow.model.*
import com.applaudotrainee.tvcatalog.domain.user.model.Avatar
import com.applaudotrainee.tvcatalog.domain.user.model.Gravatar
import com.applaudotrainee.tvcatalog.domain.user.model.Tmdb
import com.applaudotrainee.tvcatalog.domain.user.model.User
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

val gravatar = Gravatar("205e460b479e2e5b48aec07710c08d50")
val tmdb = Tmdb("/avatarPath")
val avatar = Avatar(gravatar, tmdb)
const val USER_ID = 1
const val USERNAME = "user1"
val user = User(avatar, USER_ID, USERNAME, USERNAME)

val show1 = TvShow(
    1,
    "show1",
    "2020-02-04",
    "description",
    "http://back/w20",
    "http://back/w21",
    8.6,
    "2020-02-04",
    1,
    1,
    true
)
val show2 = TvShow(
    2,
    "show2",
    "2020-02-04",
    "description",
    "http://back/w20",
    "http://back/w21",
    8.6,
    "2020-02-04",
    1,
    1,
    true
)

const val showId = 1

val showList = listOf<TvShow>(show1, show2)
val showDb = mutableListOf(showList)
val tokenResponse = TokenResponse(true, "3afgty78pl55kjlh44", "2021-02-16")
val sessionResponse = SessionResponse(true, "SUCCESS")
val cast = listOf(Cast(1, "cast", "/dsfidsp86fsd"))
val season = listOf(Season(1, "Seson 1", 1, "/sadpef8dsfs3"))
val creator = listOf(Creator(1, "creator"))
val showWithAddon = ShowWithAddon(show1, season, cast, creator)
val episode = listOf(Episode(1, "episode1", "episode1", 1, "/pdssdf845df", 1, 8.6))
val seasonWithEpisode = listOf(SeasonWithEpisode(season.first(), episode))

val flowSeasonWithEpisode = flow { emit(seasonWithEpisode)}

val accountState = AccountStateResponse(1, true)

val favoriteResponse = FavoriteResponse(1, "SUCCESS")