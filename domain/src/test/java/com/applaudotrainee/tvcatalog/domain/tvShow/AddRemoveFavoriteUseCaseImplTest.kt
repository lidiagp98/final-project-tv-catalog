package com.applaudotrainee.tvcatalog.domain.tvShow

import com.applaudotrainee.tvcatalog.domain.favoriteResponse
import com.applaudotrainee.tvcatalog.domain.showId
import com.applaudotrainee.tvcatalog.domain.tvshow.interaction.AddRemoveFavoriteUseCaseImpl
import com.applaudotrainee.tvcatalog.domain.tvshow.repository.FavoriteRepository
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito

class AddRemoveFavoriteUseCaseImplTest() {
    private val favoriteRepositoryMock = Mockito.mock(FavoriteRepository::class.java)
    private val addRemoveFavoriteUseCase = AddRemoveFavoriteUseCaseImpl(favoriteRepositoryMock)

    @Before
    fun mockFunctionality() {
        runBlocking {
            Mockito.`when`(favoriteRepositoryMock.addRemoveFavorite(showId)).thenReturn(favoriteResponse)
        }

    }

    @Test
    fun addRemoveFavorite_successfully_thenReturnFavoriteResponse() {
        runBlocking {
            val response = addRemoveFavoriteUseCase(showId)
            assertEquals(favoriteResponse, response)
        }
    }
}