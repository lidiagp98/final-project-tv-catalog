package com.applaudotrainee.tvcatalog.domain.session

import com.applaudotrainee.tvcatalog.domain.accountState
import com.applaudotrainee.tvcatalog.domain.showId
import com.applaudotrainee.tvcatalog.domain.tvshow.interaction.GetAccountStateUseCaseImpl
import com.applaudotrainee.tvcatalog.domain.tvshow.repository.FavoriteRepository
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito

class GetAccountStateUseCaseImplTest() {
    private val favoriteRepositoryMock = Mockito.mock(FavoriteRepository::class.java)
    private val getAccountStateUseCase = GetAccountStateUseCaseImpl(favoriteRepositoryMock)

    @Before
    fun mockFunctionality() {
        runBlocking {
            Mockito.`when`(favoriteRepositoryMock.getAccountState(showId)).thenReturn(accountState)
        }

    }

    @Test
    fun getAccountState_successfully_thenReturnAccountStateResponse() {
        runBlocking {
            val response = getAccountStateUseCase(showId)
            assertEquals(accountState, response)
        }
    }
}