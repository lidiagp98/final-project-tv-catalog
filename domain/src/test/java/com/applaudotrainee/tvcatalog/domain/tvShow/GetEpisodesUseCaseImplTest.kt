package com.applaudotrainee.tvcatalog.domain.tvShow

import com.applaudotrainee.tvcatalog.domain.flowSeasonWithEpisode
import com.applaudotrainee.tvcatalog.domain.showId
import com.applaudotrainee.tvcatalog.domain.tvshow.interaction.GetEpisodesUseCaseImpl
import com.applaudotrainee.tvcatalog.domain.tvshow.repository.EpisodeRepository
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito

class GetEpisodesUseCaseImplTest() {
    private val episodeRepositoryMock = Mockito.mock(EpisodeRepository::class.java)
    private val getEpisodeUseCase = GetEpisodesUseCaseImpl(episodeRepositoryMock)

    @Before
    fun mockFunctionality() {
        runBlocking {
            Mockito.`when`(episodeRepositoryMock.getEpisodes(showId)).thenReturn(flowSeasonWithEpisode)
        }

    }

    @Test
    fun getEpisodes_successfully_thenReturnSeasonWithEpisodes() {
        runBlocking {
            val response = getEpisodeUseCase(showId)
            assertEquals(flowSeasonWithEpisode, response)
        }
    }
}