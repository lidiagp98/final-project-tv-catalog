package com.applaudotrainee.tvcatalog.domain.user

import com.applaudotrainee.tvcatalog.domain.user
import com.applaudotrainee.tvcatalog.domain.user.interaction.GetUserProfileUseCaseImpl
import com.applaudotrainee.tvcatalog.domain.user.repository.UserRepository
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock

class GetUserProfileUseCaseImplTest() {
    private val userRepositoryMock = mock(UserRepository::class.java)
    private val getUserProfileUseCase = GetUserProfileUseCaseImpl(userRepositoryMock)

    @Before
    fun mockFunctionality() {
        runBlocking {
            `when`(userRepositoryMock.getUserProfile()).thenReturn(user)
        }

    }

    @Test
    fun getUserProfile_successfully_thenReturnUser() {
        runBlocking {
            val response = getUserProfileUseCase()
            assertEquals(user, response)
        }
    }
}