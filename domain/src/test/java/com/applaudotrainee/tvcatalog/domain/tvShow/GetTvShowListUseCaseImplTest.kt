package com.applaudotrainee.tvcatalog.domain.tvShow

import com.applaudotrainee.tvcatalog.domain.showList
import com.applaudotrainee.tvcatalog.domain.tvshow.interaction.GetTvShowListUseCaseImpl
import com.applaudotrainee.tvcatalog.domain.tvshow.repository.ShowRepository
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito

class GetTvShowListUseCaseImplTest() {
    private val showRepositoryMock = Mockito.mock(ShowRepository::class.java) as ShowRepository<Any>
    private val getTvShowListUseCase = GetTvShowListUseCaseImpl(showRepositoryMock)

    @Before
    fun mockFunctionality() {
        runBlocking {
            Mockito.`when`(showRepositoryMock.getShowsList()).thenReturn(showList)
        }

    }

    @Test
    fun getShowList_successfully_thenShowList() {
        runBlocking {
            val response = getTvShowListUseCase()
            assertEquals(showList, response)
        }
    }
}