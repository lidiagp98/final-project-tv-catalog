package com.applaudotrainee.tvcatalog.domain.tvShow

import com.applaudotrainee.tvcatalog.domain.showId
import com.applaudotrainee.tvcatalog.domain.showWithAddon
import com.applaudotrainee.tvcatalog.domain.tvshow.interaction.GetShowDetailsUseCaseImpl
import com.applaudotrainee.tvcatalog.domain.tvshow.repository.DetailRepository
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito

class GetShowDetailsUseCaseImplTest() {
    private val detailsRepositoryMock = Mockito.mock(DetailRepository::class.java)
    private val getShowDetailsUseCase = GetShowDetailsUseCaseImpl(detailsRepositoryMock)

    @Before
    fun mockFunctionality() {
        runBlocking {
            Mockito.`when`(detailsRepositoryMock.getShowDetails(1)).thenReturn(showWithAddon)
        }

    }

    @Test
    fun getShowDetails_successfully_thenShowDetails() {
        runBlocking {
            val response = getShowDetailsUseCase(showId)
            assertEquals(showWithAddon, response)
        }
    }
}