package com.applaudotrainee.tvcatalog.domain.user

import com.applaudotrainee.tvcatalog.domain.showList
import com.applaudotrainee.tvcatalog.domain.user.interaction.GetFavoriteShowsUseCaseImpl
import com.applaudotrainee.tvcatalog.domain.user.repository.FavoriteListRepository
import kotlinx.coroutines.runBlocking
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import org.mockito.Mockito.`when`

class GetFavoriteShowsUseCaseImplTest() {
    private val favoriteRepositoryMock = Mockito.mock(FavoriteListRepository::class.java) as FavoriteListRepository<Any>
    private val getFavoriteShowsUseCase = GetFavoriteShowsUseCaseImpl(favoriteRepositoryMock)

    @Before
    fun mockFunctionality() {
        runBlocking {
            `when`(favoriteRepositoryMock.getFavoritesList()).thenReturn(showList)
        }

    }

    @Test
    fun getFavoritesList_successfully_thenReturnFavoritesList() {
        runBlocking {
            val response = getFavoriteShowsUseCase()
            assertEquals(showList, response)
        }
    }
}