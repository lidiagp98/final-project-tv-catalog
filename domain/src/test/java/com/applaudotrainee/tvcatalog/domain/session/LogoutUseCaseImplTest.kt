package com.applaudotrainee.tvcatalog.domain.session

import com.applaudotrainee.tvcatalog.domain.session.interaction.LogoutUseCaseImpl
import com.applaudotrainee.tvcatalog.domain.session.repository.SessionRepository
import com.applaudotrainee.tvcatalog.domain.showDb
import kotlinx.coroutines.runBlocking
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito

class LogoutUseCaseImplTest() {
    private val sessionRepositoryMock = Mockito.mock(SessionRepository::class.java)
    private val logoutUseCase = LogoutUseCaseImpl(sessionRepositoryMock)

    @Before
    fun mockFunctionality() {
        runBlocking {
            Mockito.`when`(sessionRepositoryMock.deleteSessionId()).then { showDb.clear() }
        }

    }

    @Test
    fun logout_successfully_thenDeleteSessionId() {
        runBlocking {
            logoutUseCase()
            assertTrue(showDb.isEmpty())
        }
    }
}