package com.applaudotrainee.tvcatalog.domain.user.model

data class User(
    val avatar: Avatar,
    val id: Int,
    val name: String,
    val username: String
)