package com.applaudotrainee.tvcatalog.domain.tvshow.interaction

import com.applaudotrainee.tvcatalog.domain.tvshow.model.AccountStateResponse

interface GetAccountStateUseCase {
    suspend operator fun invoke(showId: Int): AccountStateResponse?
}