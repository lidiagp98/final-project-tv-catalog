package com.applaudotrainee.tvcatalog.domain.user.interaction

import com.applaudotrainee.tvcatalog.domain.user.model.User

interface GetUserProfileUseCase {
    suspend operator fun invoke(): User
}