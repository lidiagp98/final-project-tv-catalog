package com.applaudotrainee.tvcatalog.domain.tvshow.model

data class Credit(
    val cast: List<Cast>?
)
