package com.applaudotrainee.tvcatalog.domain.tvshow.model

data class Cast(
    val castId: Int,
    val name: String,
    val profilePath: String?
)
