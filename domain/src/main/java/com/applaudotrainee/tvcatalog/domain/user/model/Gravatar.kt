package com.applaudotrainee.tvcatalog.domain.user.model

data class Gravatar(
    val hash: String
)