package com.applaudotrainee.tvcatalog.domain.tvshow.model

data class FavoriteBody(
    val mediaType: String = "tv",
    val mediaId: Int,
    val favorite: Boolean
)