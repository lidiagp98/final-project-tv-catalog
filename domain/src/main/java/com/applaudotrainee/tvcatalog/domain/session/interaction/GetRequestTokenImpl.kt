package com.applaudotrainee.tvcatalog.domain.session.interaction

import com.applaudotrainee.tvcatalog.domain.session.model.TokenResponse
import com.applaudotrainee.tvcatalog.domain.session.repository.SessionRepository

class GetRequestTokenImpl(private val sessionRepository: SessionRepository) : GetRequestToken {
    override suspend fun invoke(): TokenResponse? = sessionRepository.getRequestToken()
}