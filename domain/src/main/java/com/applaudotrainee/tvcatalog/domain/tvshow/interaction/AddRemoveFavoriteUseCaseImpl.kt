package com.applaudotrainee.tvcatalog.domain.tvshow.interaction

import com.applaudotrainee.tvcatalog.domain.tvshow.model.FavoriteResponse
import com.applaudotrainee.tvcatalog.domain.tvshow.repository.FavoriteRepository

class AddRemoveFavoriteUseCaseImpl(private val favoriteRepository: FavoriteRepository) : AddRemoveFavoriteUseCase {
    override suspend fun invoke(showId: Int): FavoriteResponse? = favoriteRepository.addRemoveFavorite(showId)
}