package com.applaudotrainee.tvcatalog.domain.session.interaction

import com.applaudotrainee.tvcatalog.domain.session.model.TokenResponse

interface GetRequestToken {
    suspend operator fun invoke(): TokenResponse?
}