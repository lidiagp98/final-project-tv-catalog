package com.applaudotrainee.tvcatalog.domain.user.interaction

interface GetFavoriteShowsUseCase<out T> {
    suspend operator fun invoke(): T
}