package com.applaudotrainee.tvcatalog.domain.tvshow.model

data class FavoriteResponse(
    val statusCode: Int,
    val statusMessage: String
)
