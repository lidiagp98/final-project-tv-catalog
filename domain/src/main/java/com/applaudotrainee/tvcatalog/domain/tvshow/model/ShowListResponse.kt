package com.applaudotrainee.tvcatalog.domain.tvshow.model

data class ShowListResponse(
    val page: Int,
    val totalResults: Int,
    val total_pages: Int,
    val tvShows: List<TvShow>
)