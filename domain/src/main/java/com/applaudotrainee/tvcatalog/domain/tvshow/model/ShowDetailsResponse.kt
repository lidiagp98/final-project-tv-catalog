package com.applaudotrainee.tvcatalog.domain.tvshow.model

data class ShowDetailsResponse(
    val showId: Int,
    val showName: String,
    val airDate: String,
    val description: String,
    val backdropUrl: String?,
    val posterUrl: String?,
    val score: Double,
    val lastAirDate: String,
    val seasonNumber: Int,
    val creatorsData: List<Creator>?,
    val seasons: List<Season>?,
    val credit: Credit?
)
