package com.applaudotrainee.tvcatalog.domain.util

enum class ShowFilter(val filterOp: String) {
    POPULAR("popular"),
    TOP_RATED("top_rated"),
    ON_TV("on_the_air"),
    AIRING_TODAY("airing_today")
}