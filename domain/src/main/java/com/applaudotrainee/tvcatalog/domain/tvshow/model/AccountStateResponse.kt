package com.applaudotrainee.tvcatalog.domain.tvshow.model

data class AccountStateResponse(
    val id: Int,
    val favorite: Boolean
)
