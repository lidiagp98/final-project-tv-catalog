package com.applaudotrainee.tvcatalog.domain.session.interaction

import com.applaudotrainee.tvcatalog.domain.session.repository.SessionRepository

class LogoutUseCaseImpl(private val sessionRepository: SessionRepository) : LogoutUseCase {
    override suspend fun invoke() = sessionRepository.deleteSessionId()
}