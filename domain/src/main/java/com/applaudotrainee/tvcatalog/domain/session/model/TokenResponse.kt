package com.applaudotrainee.tvcatalog.domain.session.model

data class TokenResponse(
        val success: Boolean,
        val requestToken: String,
        val expiresAt: String
)
