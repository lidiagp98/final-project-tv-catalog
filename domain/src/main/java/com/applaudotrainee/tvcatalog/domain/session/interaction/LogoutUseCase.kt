package com.applaudotrainee.tvcatalog.domain.session.interaction

interface LogoutUseCase {
    suspend operator fun invoke()
}