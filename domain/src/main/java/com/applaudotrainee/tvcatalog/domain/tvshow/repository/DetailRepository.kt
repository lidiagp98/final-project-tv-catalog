package com.applaudotrainee.tvcatalog.domain.tvshow.repository

import com.applaudotrainee.tvcatalog.domain.tvshow.model.ShowWithAddon

interface DetailRepository {
    suspend fun getShowDetails(showId: Int): ShowWithAddon
}