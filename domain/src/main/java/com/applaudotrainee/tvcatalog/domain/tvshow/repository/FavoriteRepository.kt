package com.applaudotrainee.tvcatalog.domain.tvshow.repository

import com.applaudotrainee.tvcatalog.domain.tvshow.model.AccountStateResponse
import com.applaudotrainee.tvcatalog.domain.tvshow.model.FavoriteResponse

interface FavoriteRepository {
    suspend fun addRemoveFavorite(showId: Int): FavoriteResponse?
    suspend fun getAccountState(showId: Int): AccountStateResponse?
}