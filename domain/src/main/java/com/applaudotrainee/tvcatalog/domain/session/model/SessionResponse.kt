package com.applaudotrainee.tvcatalog.domain.session.model

data class SessionResponse(
    val success: Boolean,
    val sessionId: String
)
