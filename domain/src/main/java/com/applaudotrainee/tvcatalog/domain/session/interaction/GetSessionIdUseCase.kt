package com.applaudotrainee.tvcatalog.domain.session.interaction

import com.applaudotrainee.tvcatalog.domain.session.model.SessionResponse

interface GetSessionIdUseCase {
    suspend operator fun invoke(accessToken: String): SessionResponse?
}