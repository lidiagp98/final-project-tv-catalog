package com.applaudotrainee.tvcatalog.domain.tvshow.model

data class Season(
    val seasonId: Int,
    val name: String,
    val seasonNumber: Int,
    val posterPath: String?
)