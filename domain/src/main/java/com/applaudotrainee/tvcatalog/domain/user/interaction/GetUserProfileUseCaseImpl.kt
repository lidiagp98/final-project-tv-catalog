package com.applaudotrainee.tvcatalog.domain.user.interaction

import com.applaudotrainee.tvcatalog.domain.user.repository.UserRepository

class GetUserProfileUseCaseImpl(private val userRepository: UserRepository) : GetUserProfileUseCase {
    override suspend fun invoke() = userRepository.getUserProfile()
}