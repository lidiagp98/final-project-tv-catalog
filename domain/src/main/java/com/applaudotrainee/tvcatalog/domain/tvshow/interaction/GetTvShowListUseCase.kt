package com.applaudotrainee.tvcatalog.domain.tvshow.interaction

import com.applaudotrainee.tvcatalog.domain.util.ShowFilter

interface GetTvShowListUseCase<out T> {
    suspend operator fun invoke(filter: ShowFilter = ShowFilter.POPULAR): T
}