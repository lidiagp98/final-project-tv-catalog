package com.applaudotrainee.tvcatalog.domain.user.repository

import com.applaudotrainee.tvcatalog.domain.util.ShowFilter

interface FavoriteListRepository<out T> {
    suspend fun getFavoritesList(): T
}