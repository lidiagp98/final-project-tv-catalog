package com.applaudotrainee.tvcatalog.domain.session.interaction

import com.applaudotrainee.tvcatalog.domain.session.model.SessionResponse
import com.applaudotrainee.tvcatalog.domain.session.repository.SessionRepository

class GetSessionIdFromDbUseCaseImpl(private val sessionRepository: SessionRepository) : GetSessionIdFromDbUseCase {
    override suspend fun invoke(): SessionResponse? = sessionRepository.getSessionIdFromDatabase()
}