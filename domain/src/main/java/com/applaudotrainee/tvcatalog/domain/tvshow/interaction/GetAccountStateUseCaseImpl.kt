package com.applaudotrainee.tvcatalog.domain.tvshow.interaction

import com.applaudotrainee.tvcatalog.domain.tvshow.model.AccountStateResponse
import com.applaudotrainee.tvcatalog.domain.tvshow.repository.FavoriteRepository

class GetAccountStateUseCaseImpl(private val favoriteRepository: FavoriteRepository) :
    GetAccountStateUseCase {
    override suspend fun invoke(showId: Int): AccountStateResponse? = favoriteRepository.getAccountState(showId)
}