package com.applaudotrainee.tvcatalog.domain.tvshow.repository

import com.applaudotrainee.tvcatalog.domain.util.ShowFilter

interface ShowRepository<out T> {
    suspend fun getShowsList(filter: ShowFilter = ShowFilter.POPULAR): T
}