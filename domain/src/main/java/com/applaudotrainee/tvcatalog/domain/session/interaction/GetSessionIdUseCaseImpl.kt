package com.applaudotrainee.tvcatalog.domain.session.interaction

import com.applaudotrainee.tvcatalog.domain.session.model.SessionResponse
import com.applaudotrainee.tvcatalog.domain.session.repository.SessionRepository

class GetSessionIdUseCaseImpl(private val sessionRepository: SessionRepository) : GetSessionIdUseCase {
    override suspend fun invoke(accessToken: String): SessionResponse? = sessionRepository.getSessionId(accessToken)
}