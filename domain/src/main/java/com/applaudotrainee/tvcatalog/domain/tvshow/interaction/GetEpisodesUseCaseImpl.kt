package com.applaudotrainee.tvcatalog.domain.tvshow.interaction

import com.applaudotrainee.tvcatalog.domain.tvshow.model.SeasonWithEpisode
import com.applaudotrainee.tvcatalog.domain.tvshow.repository.EpisodeRepository
import kotlinx.coroutines.flow.Flow

class GetEpisodesUseCaseImpl(private val episodeRepository: EpisodeRepository) :
    GetEpisodesUseCase {
    override fun invoke(showId: Int): Flow<List<SeasonWithEpisode>> = episodeRepository.getEpisodes(
        showId
    )
}