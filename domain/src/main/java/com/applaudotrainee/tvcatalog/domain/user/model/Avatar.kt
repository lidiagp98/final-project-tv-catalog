package com.applaudotrainee.tvcatalog.domain.user.model

data class Avatar(
    val gravatar: Gravatar,
    val tmdb: Tmdb
)