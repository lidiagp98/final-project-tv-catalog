package com.applaudotrainee.tvcatalog.domain.tvshow.model

data class EpisodeResponse(
    val seasonNumber: Int,
    val seasonId: Int,
    val episodes: List<Episode>
)
