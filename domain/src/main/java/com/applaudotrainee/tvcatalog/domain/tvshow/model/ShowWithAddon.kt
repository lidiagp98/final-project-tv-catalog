package com.applaudotrainee.tvcatalog.domain.tvshow.model

data class ShowWithAddon(
    val show: TvShow,
    val seasonList: List<Season>?,
    val castList: List<Cast>?,
    val creatorList: List<Creator>?
)
