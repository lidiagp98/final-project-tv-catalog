package com.applaudotrainee.tvcatalog.domain.session.model

data class SessionBody(
    val access_token: String
)