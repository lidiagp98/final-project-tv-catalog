package com.applaudotrainee.tvcatalog.domain.di

import com.applaudotrainee.tvcatalog.domain.user.interaction.GetUserProfileUseCase
import com.applaudotrainee.tvcatalog.domain.session.interaction.*
import com.applaudotrainee.tvcatalog.domain.tvshow.interaction.*
import com.applaudotrainee.tvcatalog.domain.user.interaction.GetFavoriteShowsUseCase
import com.applaudotrainee.tvcatalog.domain.user.interaction.GetFavoriteShowsUseCaseImpl
import com.applaudotrainee.tvcatalog.domain.user.interaction.GetUserProfileUseCaseImpl
import org.koin.dsl.module

val interactionModule = module {
    factory<GetRequestToken> { GetRequestTokenImpl(sessionRepository = get()) }
    factory<GetSessionIdUseCase> { GetSessionIdUseCaseImpl(sessionRepository = get()) }
    factory<GetSessionIdFromDbUseCase> { GetSessionIdFromDbUseCaseImpl(sessionRepository = get()) }
    factory<GetUserProfileUseCase> { GetUserProfileUseCaseImpl(userRepository = get()) }
    factory<GetFavoriteShowsUseCase<Any>> { GetFavoriteShowsUseCaseImpl(favoriteListRepository = get()) }
    factory<GetTvShowListUseCase<Any>> { GetTvShowListUseCaseImpl(showRepository = get()) }
    factory<GetShowDetailsUseCase> { GetShowDetailsUseCaseImpl(detailRepository = get()) }
    factory<GetEpisodesUseCase> { GetEpisodesUseCaseImpl(episodeRepository = get()) }
    factory<AddRemoveFavoriteUseCase> { AddRemoveFavoriteUseCaseImpl(favoriteRepository = get()) }
    factory<GetAccountStateUseCase> { GetAccountStateUseCaseImpl(favoriteRepository = get()) }
    factory<LogoutUseCase> { LogoutUseCaseImpl(sessionRepository = get()) }
}