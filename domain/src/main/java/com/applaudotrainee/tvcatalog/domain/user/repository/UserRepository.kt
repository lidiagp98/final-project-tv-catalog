package com.applaudotrainee.tvcatalog.domain.user.repository

import com.applaudotrainee.tvcatalog.domain.tvshow.model.ShowListResponse
import com.applaudotrainee.tvcatalog.domain.user.model.User

interface UserRepository {
    suspend fun getUserProfile(): User
}