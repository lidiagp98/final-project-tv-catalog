package com.applaudotrainee.tvcatalog.domain.tvshow.model

data class SeasonWithEpisode(
    val season: Season,
    val episodeList: List<Episode>
)
