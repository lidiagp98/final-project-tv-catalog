package com.applaudotrainee.tvcatalog.domain.tvshow.repository

import com.applaudotrainee.tvcatalog.domain.tvshow.model.SeasonWithEpisode
import kotlinx.coroutines.flow.Flow

interface EpisodeRepository {
    fun getEpisodes(showId: Int): Flow<List<SeasonWithEpisode>>
}