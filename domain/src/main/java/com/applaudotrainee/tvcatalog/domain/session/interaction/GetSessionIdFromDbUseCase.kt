package com.applaudotrainee.tvcatalog.domain.session.interaction

import com.applaudotrainee.tvcatalog.domain.session.model.SessionResponse

interface GetSessionIdFromDbUseCase {
    suspend operator fun invoke(): SessionResponse?
}