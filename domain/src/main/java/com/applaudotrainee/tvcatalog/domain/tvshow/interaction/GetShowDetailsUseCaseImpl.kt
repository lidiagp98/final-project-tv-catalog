package com.applaudotrainee.tvcatalog.domain.tvshow.interaction

import com.applaudotrainee.tvcatalog.domain.tvshow.model.ShowWithAddon
import com.applaudotrainee.tvcatalog.domain.tvshow.repository.DetailRepository

class GetShowDetailsUseCaseImpl(private val detailRepository: DetailRepository) : GetShowDetailsUseCase {
    override suspend fun invoke(showId: Int): ShowWithAddon = detailRepository.getShowDetails(showId)
}