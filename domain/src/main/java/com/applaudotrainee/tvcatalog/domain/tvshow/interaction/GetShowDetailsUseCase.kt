package com.applaudotrainee.tvcatalog.domain.tvshow.interaction

import com.applaudotrainee.tvcatalog.domain.tvshow.model.ShowWithAddon

interface GetShowDetailsUseCase {
    suspend operator fun invoke(showId: Int): ShowWithAddon
}