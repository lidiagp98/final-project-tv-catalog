package com.applaudotrainee.tvcatalog.domain.tvshow.model

data class Creator(
    val creatorId: Int,
    val name: String,

)