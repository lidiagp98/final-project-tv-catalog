package com.applaudotrainee.tvcatalog.domain.tvshow.model

data class Episode(
    val id: Int,
    val name: String,
    val overview: String,
    val episodeNumber: Int,
    val stillPath: String?,
    val seasonNumber: Int,
    val voteAverage: Double
)