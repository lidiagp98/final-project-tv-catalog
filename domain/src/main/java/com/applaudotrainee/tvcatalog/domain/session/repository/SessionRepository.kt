package com.applaudotrainee.tvcatalog.domain.session.repository

import com.applaudotrainee.tvcatalog.domain.session.model.SessionResponse
import com.applaudotrainee.tvcatalog.domain.session.model.TokenResponse

interface SessionRepository {
    suspend fun getRequestToken(): TokenResponse?
    suspend fun getSessionId(accessToken: String): SessionResponse?
    suspend fun getSessionIdFromDatabase(): SessionResponse?
    suspend fun deleteSessionId()
}