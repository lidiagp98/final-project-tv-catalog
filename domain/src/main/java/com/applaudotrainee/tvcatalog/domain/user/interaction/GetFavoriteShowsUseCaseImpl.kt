package com.applaudotrainee.tvcatalog.domain.user.interaction

import com.applaudotrainee.tvcatalog.domain.tvshow.model.ShowListResponse
import com.applaudotrainee.tvcatalog.domain.tvshow.repository.FavoriteRepository
import com.applaudotrainee.tvcatalog.domain.user.repository.FavoriteListRepository
import com.applaudotrainee.tvcatalog.domain.user.repository.UserRepository

class GetFavoriteShowsUseCaseImpl<T: Any>(private val favoriteListRepository: FavoriteListRepository<T>) : GetFavoriteShowsUseCase<T> {
    override suspend fun invoke(): T = favoriteListRepository.getFavoritesList()
}