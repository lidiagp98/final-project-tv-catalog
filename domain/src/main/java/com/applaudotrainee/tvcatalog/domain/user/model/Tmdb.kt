package com.applaudotrainee.tvcatalog.domain.user.model

data class Tmdb(
    val avatar_path: String?
)