package com.applaudotrainee.tvcatalog.domain.tvshow.interaction

import com.applaudotrainee.tvcatalog.domain.tvshow.repository.ShowRepository
import com.applaudotrainee.tvcatalog.domain.util.ShowFilter

class GetTvShowListUseCaseImpl<T: Any>(private val showRepository: ShowRepository<T>) : GetTvShowListUseCase<T> {
    override suspend fun invoke(filter: ShowFilter): T = showRepository.getShowsList(filter)
}