package com.applaudotrainee.tvcatalog.domain.tvshow.interaction

import com.applaudotrainee.tvcatalog.domain.tvshow.model.FavoriteResponse

interface AddRemoveFavoriteUseCase {
    suspend operator fun invoke(showId: Int): FavoriteResponse?
}