package com.applaudotrainee.tvcatalog.domain.tvshow.interaction

import com.applaudotrainee.tvcatalog.domain.tvshow.model.SeasonWithEpisode
import kotlinx.coroutines.flow.Flow

interface GetEpisodesUseCase {
    operator fun invoke(showId: Int) : Flow<List<SeasonWithEpisode>>
}