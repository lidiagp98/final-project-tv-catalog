package com.applaudotrainee.tvcatalog.data.tvshow.relations

import androidx.room.Embedded
import androidx.room.Relation
import com.applaudotrainee.tvcatalog.data.tvshow.model.CastData
import com.applaudotrainee.tvcatalog.data.tvshow.model.CreatorData
import com.applaudotrainee.tvcatalog.data.tvshow.model.SeasonData
import com.applaudotrainee.tvcatalog.data.tvshow.model.TvShowData
import com.applaudotrainee.tvcatalog.data.util.DomainMapper
import com.applaudotrainee.tvcatalog.domain.tvshow.model.ShowWithAddon

data class ShowWithAddonData(
    @Embedded val show: TvShowData,
    @Relation(
        parentColumn = "showId",
        entityColumn = "seasonShowId"
    )
    val seasonList: List<SeasonData>?,
    @Relation(
        parentColumn = "showId",
        entityColumn = "castShowId"
    )
    val castList: List<CastData>?,
    @Relation(
        parentColumn = "showId",
        entityColumn = "creatorShowId"
    )
    val creatorList: List<CreatorData>?

) : DomainMapper<ShowWithAddon> {
    override fun mapToDomainModel() = ShowWithAddon(
        show.mapToDomainModel(),
        seasonList?.map { it.mapToDomainModel() },
        castList?.map { it.mapToDomainModel() },
        creatorList?.map { it.mapToDomainModel() }
    )
}
