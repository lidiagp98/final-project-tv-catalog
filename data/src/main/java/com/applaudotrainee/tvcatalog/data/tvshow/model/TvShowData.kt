package com.applaudotrainee.tvcatalog.data.tvshow.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.applaudotrainee.tvcatalog.data.util.DomainMapper
import com.applaudotrainee.tvcatalog.domain.tvshow.model.TvShow
import com.google.gson.annotations.SerializedName

@Entity(tableName = "TvShow")
data class TvShowData(
    @PrimaryKey
    @field:SerializedName("id") val showId: Int,
    @field:SerializedName("name")
    val showName: String,
    @field:SerializedName("first_air_date") val airDate: String,
    @field:SerializedName("overview") val description: String?,
    @field:SerializedName("backdrop_path") val backdropUrl: String?,
    @field:SerializedName("poster_path") val posterUrl: String?,
    @field:SerializedName("vote_average") val score: Double,
    @field:SerializedName("last_air_date") val lastAirDate: String?,
    @field:SerializedName("number_of_seasons") val seasonNumber: Int?,
    var order: Int,
    var favorite: Boolean?
) : DomainMapper<TvShow> {
    override fun mapToDomainModel() =
        TvShow(showId, showName, airDate, description, backdropUrl, posterUrl, score, lastAirDate, seasonNumber, order, favorite)
}