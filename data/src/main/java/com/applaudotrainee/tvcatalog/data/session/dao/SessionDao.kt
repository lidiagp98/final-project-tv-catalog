package com.applaudotrainee.tvcatalog.data.session.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.applaudotrainee.tvcatalog.data.session.model.SessionResponseData

@Dao
interface SessionDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertSessionId(sessionResponseData: SessionResponseData)

    @Query("DELETE FROM Session")
    suspend fun deleteSessionId()

    @Query("SELECT * FROM Session")
    suspend fun getSessionId(): SessionResponseData?

    @Query("SELECT * FROM Session")
    suspend fun getSessionIdFromDb(): SessionResponseData

}