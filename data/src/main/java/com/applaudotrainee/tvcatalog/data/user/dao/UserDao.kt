package com.applaudotrainee.tvcatalog.data.user.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.applaudotrainee.tvcatalog.data.user.model.UserData

@Dao
interface UserDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertUser(user: UserData)

    @Query("DELETE FROM User")
    suspend fun deleteUser()

    @Query("SELECT * FROM User")
    suspend fun getUser() : UserData
}