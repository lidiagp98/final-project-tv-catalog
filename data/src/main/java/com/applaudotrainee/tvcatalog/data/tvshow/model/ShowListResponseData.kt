package com.applaudotrainee.tvcatalog.data.tvshow.model

import com.applaudotrainee.tvcatalog.data.util.DomainMapper
import com.applaudotrainee.tvcatalog.domain.tvshow.model.ShowListResponse
import com.google.gson.annotations.SerializedName

data class ShowListResponseData(
    @field:SerializedName("page") val page: Int,
    @field:SerializedName("total_results") val totalResults: Int,
    @field:SerializedName("total_pages") val total_pages: Int,
    @field:SerializedName("results") val tvShows: List<TvShowData>
) : DomainMapper<ShowListResponse> {
    override fun mapToDomainModel() =
        ShowListResponse(page, totalResults, total_pages, tvShows.map { it.mapToDomainModel() })
}