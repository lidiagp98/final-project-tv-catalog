package com.applaudotrainee.tvcatalog.data.di

import com.applaudotrainee.tvcatalog.data.session.repository.SessionRepositoryIml
import com.applaudotrainee.tvcatalog.data.tvshow.repository.DetailRepositoryImpl
import com.applaudotrainee.tvcatalog.data.tvshow.repository.EpisodeRepositoryImpl
import com.applaudotrainee.tvcatalog.data.tvshow.repository.FavoriteRepositoryImpl
import com.applaudotrainee.tvcatalog.data.tvshow.repository.ShowRepositoryImpl
import com.applaudotrainee.tvcatalog.data.user.repository.FavoriteListRepositoryImpl
import com.applaudotrainee.tvcatalog.data.user.repository.UserRepositoryImpl
import com.applaudotrainee.tvcatalog.data.util.Connectivity
import com.applaudotrainee.tvcatalog.data.util.ConnectivityImpl
import com.applaudotrainee.tvcatalog.domain.session.repository.SessionRepository
import com.applaudotrainee.tvcatalog.domain.tvshow.repository.DetailRepository
import com.applaudotrainee.tvcatalog.domain.tvshow.repository.EpisodeRepository
import com.applaudotrainee.tvcatalog.domain.tvshow.repository.FavoriteRepository
import com.applaudotrainee.tvcatalog.domain.tvshow.repository.ShowRepository
import com.applaudotrainee.tvcatalog.domain.user.repository.FavoriteListRepository
import com.applaudotrainee.tvcatalog.domain.user.repository.UserRepository
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val repositoryModule = module {
    factory<SessionRepository> { SessionRepositoryIml(movieAPI = get(), sessionDao = get()) }
    factory<UserRepository> {
        UserRepositoryImpl(
            movieDbApi = get(),
            sessionDao = get(),
            userDao = get()
        )
    }
    factory<ShowRepository<Any>> { ShowRepositoryImpl(movieDbApi = get(), showDao = get()) }
    factory<FavoriteListRepository<Any>> { FavoriteListRepositoryImpl(movieDbApi = get(), showDao = get(), sessionDao = get()) }
    factory<DetailRepository> {
        DetailRepositoryImpl(
            movieDbApi = get(),
            tvShowDao = get(),
            castDao = get(),
            creatorDao = get(),
            seasonDao = get()
        )
    }
    factory<EpisodeRepository> { EpisodeRepositoryImpl(movieDbApi = get(), episodeDao = get(), seasonDao = get()) }
    factory<FavoriteRepository> { FavoriteRepositoryImpl(movieDbApi = get(), sessionDao = get()) }

    single<Connectivity> { ConnectivityImpl(androidContext()) }
}