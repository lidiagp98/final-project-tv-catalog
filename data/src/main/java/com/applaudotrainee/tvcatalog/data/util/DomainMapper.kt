package com.applaudotrainee.tvcatalog.data.util

interface DomainMapper<T : Any> {
    fun mapToDomainModel(): T
}