package com.applaudotrainee.tvcatalog.data.tvshow.repository

import com.applaudotrainee.tvcatalog.data.network.MovieDbApi
import com.applaudotrainee.tvcatalog.data.tvshow.dao.EpisodeDao
import com.applaudotrainee.tvcatalog.data.tvshow.dao.SeasonDao
import com.applaudotrainee.tvcatalog.data.tvshow.model.EpisodeData
import com.applaudotrainee.tvcatalog.data.tvshow.model.SeasonData
import com.applaudotrainee.tvcatalog.data.util.Connectivity
import com.applaudotrainee.tvcatalog.domain.tvshow.model.SeasonWithEpisode
import com.applaudotrainee.tvcatalog.domain.tvshow.repository.EpisodeRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.runBlocking
import org.koin.java.KoinJavaComponent

class EpisodeRepositoryImpl(
    private val movieDbApi: MovieDbApi,
    private val episodeDao: EpisodeDao,
    private val seasonDao: SeasonDao
) :
    EpisodeRepository {
    private val connectivity: Connectivity by KoinJavaComponent.inject(Connectivity::class.java)

    override fun getEpisodes(showId: Int): Flow<List<SeasonWithEpisode>> {

        val seasons = runBlocking { seasonDao.getSeasonsList(showId) }

        getAndInsertEpisodes(seasons, showId)

        val seasonsIds = seasons.map { it.seasonId }

        return seasonDao.getSeasonWithEpisodes(seasonsIds).map { list ->
            list.map { seasonWithEpisodeData ->
                seasonWithEpisodeData.episodeList = seasonWithEpisodeData.episodeList.sortedBy { it.episodeNumber }
                seasonWithEpisodeData.mapToDomainModel()
            }
        }
    }

    private fun getAndInsertEpisodes(seasons: List<SeasonData>, showId: Int) {
        val episodes = mutableListOf<EpisodeData>()
        if (connectivity.isNetworkConnected.value == true){
            runBlocking {
                seasons.forEach { season ->
                    val response =
                        movieDbApi.getEpisodes(showId = showId, seasonNumber = season.seasonNumber)

                    response.episodes.map { it.episodeSeasonId = season.seasonId }

                    episodes.addAll(response.episodes)
                }
                episodeDao.insertEpisodes(episodes)
            }
        }
    }

}