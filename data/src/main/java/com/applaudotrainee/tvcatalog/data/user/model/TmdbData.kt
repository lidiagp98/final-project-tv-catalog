package com.applaudotrainee.tvcatalog.data.user.model

import com.applaudotrainee.tvcatalog.data.util.DomainMapper
import com.applaudotrainee.tvcatalog.domain.user.model.Tmdb
import com.google.gson.annotations.SerializedName

data class TmdbData(

    @field:SerializedName("avatar_path") val avatar_path: String?

) : DomainMapper<Tmdb> {
    override fun mapToDomainModel() = Tmdb(avatar_path)
}