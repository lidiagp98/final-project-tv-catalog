package com.applaudotrainee.tvcatalog.data.tvshow.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.applaudotrainee.tvcatalog.data.tvshow.model.CastData
import kotlinx.coroutines.flow.Flow

@Dao
interface CastDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertCast(cast: List<CastData>)

    @Query("SELECT * FROM ShowCast WHERE castShowId = :castShowId")
    fun getCastList(castShowId: Int): Flow<List<CastData>>

}