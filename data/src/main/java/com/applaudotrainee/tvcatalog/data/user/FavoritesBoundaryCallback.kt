package com.applaudotrainee.tvcatalog.data.user

import android.util.Log
import androidx.paging.PagedList
import com.applaudotrainee.tvcatalog.data.network.MovieDbApi
import com.applaudotrainee.tvcatalog.data.session.dao.SessionDao
import com.applaudotrainee.tvcatalog.data.tvshow.dao.TvShowDao
import com.applaudotrainee.tvcatalog.data.tvshow.model.ShowListResponseData
import com.applaudotrainee.tvcatalog.data.util.Connectivity
import com.applaudotrainee.tvcatalog.domain.tvshow.model.TvShow
import kotlinx.coroutines.runBlocking
import org.koin.java.KoinJavaComponent
import java.net.UnknownHostException

class FavoritesBoundaryCallback(
    private val movieDbApi: MovieDbApi,
    private val sessionDao: SessionDao,
    private val showDao: TvShowDao
) :
    PagedList.BoundaryCallback<TvShow>() {

    private val connectivity: Connectivity by KoinJavaComponent.inject(Connectivity::class.java)
    private var currentPage = 1

    init {
        firstLoad()
    }

    private fun firstLoad() {
        runBlocking {
            if (connectivity.isNetworkConnected.value == true) {
                try {
                    val sessionId = sessionDao.getSessionId()?.sessionId ?: ""

                    val showsFromApi = movieDbApi.getFavoritesList(session_id = sessionId)

                    currentPage = showsFromApi.page

                    showDao.insertTvShows(markTvShowAsFavorite(showsFromApi).tvShows)
                } catch (e: UnknownHostException) {
                    Log.i("Http Error", e.message.toString())
                }
            }
        }
    }

    override fun onItemAtEndLoaded(itemAtEnd: TvShow) {
        runBlocking {
            if (connectivity.isNetworkConnected.value == true) {
                try {
                    val sessionId = sessionDao.getSessionId()?.sessionId ?: ""
                    val showsFromApi =
                        movieDbApi.getFavoritesList(session_id = sessionId, page = currentPage + 1)

                    currentPage = showsFromApi.page

                    showDao.insertTvShows(markTvShowAsFavorite(showsFromApi).tvShows)

                } catch (e: UnknownHostException) {
                    Log.i("Http Error", e.message.toString())
                }
            }
        }
    }

    private fun markTvShowAsFavorite(showsFromApi: ShowListResponseData): ShowListResponseData {
        showsFromApi.tvShows.map {
            it.favorite = true
        }
        return showsFromApi
    }
}
