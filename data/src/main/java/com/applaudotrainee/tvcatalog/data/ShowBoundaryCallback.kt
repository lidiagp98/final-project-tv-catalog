package com.applaudotrainee.tvcatalog.data

import android.util.Log
import androidx.paging.PagedList
import com.applaudotrainee.tvcatalog.data.network.MovieDbApi
import com.applaudotrainee.tvcatalog.data.tvshow.dao.TvShowDao
import com.applaudotrainee.tvcatalog.data.tvshow.model.ShowListResponseData
import com.applaudotrainee.tvcatalog.data.util.Connectivity
import com.applaudotrainee.tvcatalog.domain.tvshow.model.TvShow
import com.applaudotrainee.tvcatalog.domain.util.ShowFilter
import kotlinx.coroutines.runBlocking
import org.koin.java.KoinJavaComponent
import java.net.UnknownHostException

class ShowBoundaryCallback(
    private val movieDbApi: MovieDbApi,
    private val showFilter: ShowFilter,
    private val showDao: TvShowDao
) :
    PagedList.BoundaryCallback<TvShow>() {

    private val connectivity: Connectivity by KoinJavaComponent.inject(Connectivity::class.java)
    private var currentPage = 1
    private var order = 0

    init {
        firstLoad()
    }

    private fun firstLoad() {
        runBlocking {
            if (connectivity.isNetworkConnected.value == true) {
                try {
                    val showsFromApi = movieDbApi.getShowsList(filter = showFilter.filterOp)
                    currentPage = showsFromApi.page
                    showDao.deleteAndInsertTvShowsDb(setOrderOfTvShow(showsFromApi).tvShows)
                } catch (e: UnknownHostException) {
                    Log.i("Http Error", e.message.toString())
                }
            }
        }
    }

    override fun onItemAtEndLoaded(itemAtEnd: TvShow) {
        runBlocking {
            if (connectivity.isNetworkConnected.value == true) {
                try {
                    val showsFromApi = movieDbApi.getShowsList(filter = showFilter.filterOp, page = currentPage + 1)
                    currentPage = showsFromApi.page
                    showDao.insertTvShows(setOrderOfTvShow(showsFromApi).tvShows)
                } catch (e: UnknownHostException) {
                    Log.i("Http Error", e.message.toString())
                }
            }
        }
    }

    private fun setOrderOfTvShow(showsFromApi: ShowListResponseData): ShowListResponseData {
        showsFromApi.tvShows.map {
            order++
            it.order = order
        }
        return showsFromApi
    }
}
