package com.applaudotrainee.tvcatalog.data.tvshow.repository

import androidx.lifecycle.LiveData
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.applaudotrainee.tvcatalog.data.ShowBoundaryCallback
import com.applaudotrainee.tvcatalog.data.network.MovieDbApi
import com.applaudotrainee.tvcatalog.data.tvshow.dao.TvShowDao
import com.applaudotrainee.tvcatalog.domain.tvshow.model.TvShow
import com.applaudotrainee.tvcatalog.domain.tvshow.repository.ShowRepository
import com.applaudotrainee.tvcatalog.domain.util.ShowFilter

class ShowRepositoryImpl(private val movieDbApi: MovieDbApi, private val showDao: TvShowDao) :
    ShowRepository<LiveData<PagedList<TvShow>>> {

    override suspend fun getShowsList(filter: ShowFilter): LiveData<PagedList<TvShow>> {

        val config = PagedList.Config.Builder()
            .setPageSize(PAGE_SIZE)
            .setInitialLoadSizeHint(PAGE_SIZE)
            .setPrefetchDistance(PREFETCH_DISTANCE)
            .setEnablePlaceholders(false)
            .build()

        return LivePagedListBuilder(showDao.getAllTvShowsFromDb().mapByPage { list ->
            list.map { it.mapToDomainModel() }
        }, config).setBoundaryCallback(ShowBoundaryCallback(movieDbApi, filter, showDao)).build()
    }

    companion object {
        private const val PAGE_SIZE = 20
        private const val PREFETCH_DISTANCE = 10
    }
}