package com.applaudotrainee.tvcatalog.data.user.model

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.applaudotrainee.tvcatalog.data.util.DomainMapper
import com.applaudotrainee.tvcatalog.domain.user.model.User
import com.google.gson.annotations.SerializedName

@Entity(tableName = "User")
data class UserData(
    @field:SerializedName("avatar")
    @Embedded
    val avatarData: AvatarData,
    @field:SerializedName("id")
    @PrimaryKey
    val id: Int,
    @field:SerializedName("name") val name: String,
    @field:SerializedName("username") val username: String
) : DomainMapper<User> {
    override fun mapToDomainModel() = User(
        avatarData.mapToDomainModel(),
        id,
        name,
        username
    )
}