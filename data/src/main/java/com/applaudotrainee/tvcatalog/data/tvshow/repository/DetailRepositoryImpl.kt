package com.applaudotrainee.tvcatalog.data.tvshow.repository

import android.util.Log
import com.applaudotrainee.tvcatalog.data.network.MovieDbApi
import com.applaudotrainee.tvcatalog.data.tvshow.dao.CastDao
import com.applaudotrainee.tvcatalog.data.tvshow.dao.CreatorDao
import com.applaudotrainee.tvcatalog.data.tvshow.dao.SeasonDao
import com.applaudotrainee.tvcatalog.data.tvshow.dao.TvShowDao
import com.applaudotrainee.tvcatalog.data.tvshow.model.ShowDetailsResponseData
import com.applaudotrainee.tvcatalog.data.tvshow.model.TvShowData
import com.applaudotrainee.tvcatalog.data.util.Connectivity
import com.applaudotrainee.tvcatalog.domain.tvshow.model.ShowWithAddon
import com.applaudotrainee.tvcatalog.domain.tvshow.repository.DetailRepository
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.runBlocking
import org.koin.java.KoinJavaComponent
import java.net.UnknownHostException

class DetailRepositoryImpl(
    private val movieDbApi: MovieDbApi,
    private val tvShowDao: TvShowDao,
    private val castDao: CastDao,
    private val seasonDao: SeasonDao,
    private val creatorDao: CreatorDao
) : DetailRepository {

    private val connectivity: Connectivity by KoinJavaComponent.inject(Connectivity::class.java)

    override suspend fun getShowDetails(showId: Int): ShowWithAddon {
        if (connectivity.isNetworkConnected.value == true) {
            try {
                val apiResponse = movieDbApi.getShowDetails(showId)
                insertAllAddons(apiResponse)
            } catch (e: UnknownHostException) {
                Log.i("Http Error", e.message.toString())
                return tvShowDao.getShowDetailsWithAddons(showId).mapToDomainModel()
            }
        }
        return tvShowDao.getShowDetailsWithAddons(showId).mapToDomainModel()
    }

    private suspend fun insertAllAddons(apiResponse: ShowDetailsResponseData) {
        val order = tvShowDao.getOrderOfTvShow(apiResponse.showId)
        val favorite = tvShowDao.getFavoriteState(apiResponse.showId)
        tvShowDao.insertTvShowDetails(
            TvShowData(
                apiResponse.showId,
                apiResponse.showName,
                apiResponse.airDate,
                apiResponse.description,
                apiResponse.backdropUrl,
                apiResponse.posterUrl,
                apiResponse.score,
                apiResponse.lastAirDate,
                apiResponse.seasonNumber,
                order,
                favorite
            )
        )
        apiResponse.credit?.let { credit ->
            credit.cast?.map { cast ->
                cast.castShowId = apiResponse.showId
            }
            credit.cast?.let { castDao.insertCast(it) }
        }

        apiResponse.seasons?.let { seasonList ->
            seasonList.map { season ->
                season.seasonShowId = apiResponse.showId
            }
            seasonDao.insertSeasons(seasonList)
        }

        apiResponse.creatorsData?.let { creatorList ->
            creatorList.map { creator ->
                creator.creatorShowId = apiResponse.showId
            }
            creatorDao.insertCreators(creatorList)
        }
    }
}