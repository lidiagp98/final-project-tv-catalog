package com.applaudotrainee.tvcatalog.data.tvshow.model

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import com.applaudotrainee.tvcatalog.data.util.DomainMapper
import com.applaudotrainee.tvcatalog.domain.tvshow.model.Creator
import com.google.gson.annotations.SerializedName

@Entity(
    tableName = "Creator", foreignKeys = [ForeignKey(
        entity = TvShowData::class,
        parentColumns = ["showId"],
        childColumns = ["creatorShowId"],
        onDelete = ForeignKey.CASCADE
    )]
)
data class CreatorData(
    @field:SerializedName("id")
    @PrimaryKey
    val creatorId: Int,

    var creatorShowId: Int,

    @field:SerializedName("name")
    val name: String,

    ) : DomainMapper<Creator> {
    override fun mapToDomainModel() = Creator(creatorId, name)
}