package com.applaudotrainee.tvcatalog.data.user.repository

import com.applaudotrainee.tvcatalog.data.network.MovieDbApi
import com.applaudotrainee.tvcatalog.data.session.dao.SessionDao
import com.applaudotrainee.tvcatalog.data.user.dao.UserDao
import com.applaudotrainee.tvcatalog.data.util.Connectivity
import com.applaudotrainee.tvcatalog.domain.tvshow.model.ShowListResponse
import com.applaudotrainee.tvcatalog.domain.user.model.User
import com.applaudotrainee.tvcatalog.domain.user.repository.UserRepository
import org.koin.java.KoinJavaComponent

class UserRepositoryImpl(
    private val movieDbApi: MovieDbApi,
    private val sessionDao: SessionDao,
    private val userDao: UserDao
) :
    UserRepository {

    private val connectivity: Connectivity by KoinJavaComponent.inject(Connectivity::class.java)

    override suspend fun getUserProfile(): User {
        val sessionId = sessionDao.getSessionId()?.sessionId ?: ""
        return if (connectivity.isNetworkConnected.value == true) {
            val user = movieDbApi.getUserProfile(session_id = sessionId)
            userDao.insertUser(user)
            user.mapToDomainModel()
        } else userDao.getUser().mapToDomainModel()
    }
}