package com.applaudotrainee.tvcatalog.data.di

import androidx.room.Room
import com.applaudotrainee.tvcatalog.data.database.TVRoomDatabase
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

private const val DATABASE_NAME = "tv_database"

val databaseModule = module {
    single {
        Room.databaseBuilder(
            androidContext().applicationContext,
            TVRoomDatabase::class.java,
            DATABASE_NAME
        ).build()
    }
    factory { get<TVRoomDatabase>().sessionDao() }
    factory { get<TVRoomDatabase>().userDao() }
    factory { get<TVRoomDatabase>().tvShowDao() }
    factory { get<TVRoomDatabase>().castDao() }
    factory { get<TVRoomDatabase>().seasonDao() }
    factory { get<TVRoomDatabase>().creatorDao() }
    factory { get<TVRoomDatabase>().episodeDao() }
}