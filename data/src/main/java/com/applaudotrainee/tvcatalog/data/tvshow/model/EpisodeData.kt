package com.applaudotrainee.tvcatalog.data.tvshow.model

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import com.applaudotrainee.tvcatalog.data.util.DomainMapper
import com.applaudotrainee.tvcatalog.domain.tvshow.model.Episode
import com.google.gson.annotations.SerializedName

@Entity(tableName = "Episode", foreignKeys = [ForeignKey(
    entity = SeasonData::class,
    parentColumns = ["seasonId"],
    childColumns = ["episodeSeasonId"],
    onDelete = ForeignKey.CASCADE
)])
data class EpisodeData(
    @field:SerializedName("id")
    @PrimaryKey
    val id: Int,

    var episodeSeasonId: Int,

    @field:SerializedName("name")
    val name: String,

    @field:SerializedName("overview")
    val overview: String,

    @field:SerializedName("episode_number")
    val episodeNumber: Int,

    @field:SerializedName("still_path")
    val stillPath: String?,

    @field:SerializedName("season_number")
    val seasonNumber: Int,

    @field:SerializedName("vote_average")
    val voteAverage: Double

) : DomainMapper<Episode> {
    override fun mapToDomainModel() =
        Episode(id, name, overview, episodeNumber, stillPath, seasonNumber, voteAverage)
}