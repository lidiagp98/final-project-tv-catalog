package com.applaudotrainee.tvcatalog.data.tvshow.model

import com.applaudotrainee.tvcatalog.data.util.DomainMapper
import com.applaudotrainee.tvcatalog.domain.tvshow.model.FavoriteResponse
import com.google.gson.annotations.SerializedName

data class FavoriteResponseData(
    @field:SerializedName("status_code") val statusCode: Int,
    @field:SerializedName("status_message") val statusMessage: String
) : DomainMapper<FavoriteResponse> {
    override fun mapToDomainModel() = FavoriteResponse(statusCode, statusMessage)
}
