package com.applaudotrainee.tvcatalog.data.tvshow.model

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.ForeignKey.CASCADE
import androidx.room.PrimaryKey
import com.applaudotrainee.tvcatalog.data.util.DomainMapper
import com.applaudotrainee.tvcatalog.domain.tvshow.model.Cast
import com.google.gson.annotations.SerializedName

@Entity(
    tableName = "ShowCast", foreignKeys = [ForeignKey(
        entity = TvShowData::class,
        parentColumns = ["showId"],
        childColumns = ["castShowId"],
        onDelete = CASCADE
    )]
)
data class CastData(
    @field:SerializedName("id")
    @PrimaryKey
    val castId: Int,

    var castShowId: Int,

    @field:SerializedName("name")
    val name: String,

    @field:SerializedName("profile_path")
    val profilePath: String?

) : DomainMapper<Cast> {
    override fun mapToDomainModel() = Cast(castId, name, profilePath)
}
