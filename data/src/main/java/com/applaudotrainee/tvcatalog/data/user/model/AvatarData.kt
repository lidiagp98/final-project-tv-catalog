package com.applaudotrainee.tvcatalog.data.user.model

import androidx.room.Embedded
import com.applaudotrainee.tvcatalog.data.util.DomainMapper
import com.applaudotrainee.tvcatalog.domain.user.model.Avatar
import com.google.gson.annotations.SerializedName

data class AvatarData(
    @field:SerializedName("gravatar")
    @Embedded
    val gravatarData: GravatarData,

    @field:SerializedName("tmdb")
    @Embedded
    val tmdbData: TmdbData

) : DomainMapper<Avatar> {
    override fun mapToDomainModel() =
        Avatar(gravatarData.mapToDomainModel(), tmdbData.mapToDomainModel())
}