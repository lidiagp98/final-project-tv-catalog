package com.applaudotrainee.tvcatalog.data.tvshow.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import com.applaudotrainee.tvcatalog.data.tvshow.model.EpisodeData

@Dao
interface EpisodeDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertEpisodes(episodes: List<EpisodeData>)
}