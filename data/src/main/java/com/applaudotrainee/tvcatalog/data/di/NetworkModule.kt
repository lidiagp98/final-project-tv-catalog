package com.applaudotrainee.tvcatalog.data.di

import com.applaudotrainee.tvcatalog.data.network.MovieDbApi
import org.koin.dsl.module
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

private const val BASE_URL = "https://api.themoviedb.org/3/"

val networkModule = module {
    single { GsonConverterFactory.create() as Converter.Factory }

    single {
        Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(get())
            .build()
    }

    single { get<Retrofit>().create(MovieDbApi::class.java) }
}