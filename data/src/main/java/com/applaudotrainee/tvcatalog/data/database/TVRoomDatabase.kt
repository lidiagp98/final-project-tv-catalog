package com.applaudotrainee.tvcatalog.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.applaudotrainee.tvcatalog.data.session.dao.SessionDao
import com.applaudotrainee.tvcatalog.data.session.model.SessionResponseData
import com.applaudotrainee.tvcatalog.data.tvshow.dao.*
import com.applaudotrainee.tvcatalog.data.tvshow.model.*
import com.applaudotrainee.tvcatalog.data.user.dao.UserDao
import com.applaudotrainee.tvcatalog.data.user.model.UserData

@Database(
    entities = [SessionResponseData::class, UserData::class, TvShowData::class, CastData::class, CreatorData::class, SeasonData::class, EpisodeData::class],
    version = 1,
    exportSchema = false
)
abstract class TVRoomDatabase : RoomDatabase() {

    abstract fun sessionDao(): SessionDao
    abstract fun userDao(): UserDao
    abstract fun tvShowDao(): TvShowDao
    abstract fun castDao(): CastDao
    abstract fun creatorDao(): CreatorDao
    abstract fun seasonDao(): SeasonDao
    abstract fun episodeDao(): EpisodeDao
}