package com.applaudotrainee.tvcatalog.data.tvshow.model

import androidx.room.PrimaryKey
import com.applaudotrainee.tvcatalog.data.util.DomainMapper
import com.applaudotrainee.tvcatalog.domain.tvshow.model.ShowDetailsResponse
import com.google.gson.annotations.SerializedName

data class ShowDetailsResponseData(
    @field:SerializedName("id")
    val showId: Int,

    @field:SerializedName("name")
    @PrimaryKey
    val showName: String,

    @field:SerializedName("first_air_date")
    val airDate: String,

    @field:SerializedName("overview")
    val description: String,

    @field:SerializedName("backdrop_path")
    val backdropUrl: String?,

    @field:SerializedName("poster_path")
    val posterUrl: String?,

    @field:SerializedName("vote_average")
    val score: Double,

    @field:SerializedName("last_air_date")
    val lastAirDate: String,

    @field:SerializedName("number_of_seasons")
    val seasonNumber: Int,

    @field:SerializedName("created_by")
    val creatorsData: List<CreatorData>?,

    @field:SerializedName("seasons")
    val seasons: List<SeasonData>?,

    @field:SerializedName("credits") val credit: CreditData?
) : DomainMapper<ShowDetailsResponse> {
    override fun mapToDomainModel() = ShowDetailsResponse(
        showId,
        showName,
        airDate,
        description,
        backdropUrl,
        posterUrl,
        score,
        lastAirDate,
        seasonNumber,
        creatorsData?.map { it.mapToDomainModel() },
        seasons?.map { it.mapToDomainModel() },
        credit?.mapToDomainModel()
    )
}
