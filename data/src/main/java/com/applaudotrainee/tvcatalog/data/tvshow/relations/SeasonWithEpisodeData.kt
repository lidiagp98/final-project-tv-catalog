package com.applaudotrainee.tvcatalog.data.tvshow.relations

import androidx.room.Embedded
import androidx.room.Relation
import com.applaudotrainee.tvcatalog.data.tvshow.model.EpisodeData
import com.applaudotrainee.tvcatalog.data.tvshow.model.SeasonData
import com.applaudotrainee.tvcatalog.data.util.DomainMapper
import com.applaudotrainee.tvcatalog.domain.tvshow.model.SeasonWithEpisode

data class SeasonWithEpisodeData(
    @Embedded val season: SeasonData,
    @Relation(
        parentColumn = "seasonId",
        entityColumn = "episodeSeasonId"
    )
    var episodeList: List<EpisodeData>
) : DomainMapper<SeasonWithEpisode> {
    override fun mapToDomainModel() =
        SeasonWithEpisode(season.mapToDomainModel(), episodeList.map { it.mapToDomainModel() })
}
