package com.applaudotrainee.tvcatalog.data.network

import com.applaudotrainee.tvcatalog.data.BuildConfig
import com.applaudotrainee.tvcatalog.data.session.model.SessionBodyData
import com.applaudotrainee.tvcatalog.data.session.model.SessionResponseData
import com.applaudotrainee.tvcatalog.data.session.model.TokenResponseData
import com.applaudotrainee.tvcatalog.data.tvshow.model.*
import com.applaudotrainee.tvcatalog.data.user.model.UserData
import com.applaudotrainee.tvcatalog.data.util.*
import retrofit2.http.*

interface MovieDbApi {

    @GET(TOKEN_URL)
    suspend fun getTokenRequest(): TokenResponseData

    @Headers("Content-Type: application/json")
    @POST(SESSION_URL)
    suspend fun getSessionId(@Body accessToken: SessionBodyData): SessionResponseData

    @GET(POPULAR_URL)
    suspend fun getShowsList(
        @Path(FILTER_PATH) filter: String,
        @Query(API_KEY_PARAMETER) api_key: String = BuildConfig.MOVIEDB_API_KEY,
        @Query(PAGE_PARAMETER) page: Int = 1
    ): ShowListResponseData

    @GET(ACCOUNT_URL)
    suspend fun getUserProfile(
        @Query(API_KEY_PARAMETER) api_key: String = BuildConfig.MOVIEDB_API_KEY,
        @Query(SESSION_PARAMETER) session_id: String
    ): UserData

    @GET(FAVORITES_URL)
    suspend fun getFavoritesList(
        @Query(API_KEY_PARAMETER) api_key: String = BuildConfig.MOVIEDB_API_KEY,
        @Query(SESSION_PARAMETER) session_id: String,
        @Query(PAGE_PARAMETER) page: Int = 1
    ): ShowListResponseData

    @GET(DETAILS_URL)
    suspend fun getShowDetails(
        @Path(SHOW_ID_PATH) showId: Int,
        @Query(API_KEY_PARAMETER) api_key: String = BuildConfig.MOVIEDB_API_KEY,
        @Query(APPEND) appendResponse: String = CREDITS
    ): ShowDetailsResponseData

    @GET(EPISODE_URL)
    suspend fun getEpisodes(
        @Path(SHOW_ID_PATH) showId: Int,
        @Path(SEASON_NUMBER) seasonNumber: Int,
        @Query(API_KEY_PARAMETER) api_key: String = BuildConfig.MOVIEDB_API_KEY
    ): EpisodeResponseData

    @POST(ADD_FAVORITE_URL)
    suspend fun addFavorite(
        @Body favoriteBody: FavoriteBodyData,
        @Query(API_KEY_PARAMETER) api_key: String = BuildConfig.MOVIEDB_API_KEY,
        @Query(SESSION_PARAMETER) session_id: String
    ): FavoriteResponseData

    @GET(ACCOUNT_STATES_URL)
    suspend fun getAccountStates(
        @Path(SHOW_ID_PATH) showId: Int,
        @Query(API_KEY_PARAMETER) api_key: String = BuildConfig.MOVIEDB_API_KEY,
        @Query(SESSION_PARAMETER) session_id: String
    ): AccountStateResponseData

}