package com.applaudotrainee.tvcatalog.data.util

import com.applaudotrainee.tvcatalog.data.BuildConfig

const val API_KEY_PARAMETER = "api_key"

const val TOKEN_URL = "authentication/token/new?api_key=${BuildConfig.MOVIEDB_API_KEY}"

const val SESSION_URL = "authentication/session/new?api_key=${BuildConfig.MOVIEDB_API_KEY}"

const val  FILTER_PATH = "filter"

const val POPULAR_URL = "tv/{$FILTER_PATH}?"

const val PAGE_PARAMETER = "page"

const val ACCOUNT_URL = "account?"

const val SESSION_PARAMETER = "session_id"

const val FAVORITES_URL = "account/{account_id}/favorite/tv?"

const val SHOW_ID_PATH= "tv_id"

const val APPEND = "append_to_response"

const val CREDITS = "credits"

const val DETAILS_URL = "tv/{$SHOW_ID_PATH}?"

const val SEASON_NUMBER = "season_number"

const val EPISODE_URL = "tv/{$SHOW_ID_PATH}/season/{$SEASON_NUMBER}?"

const val ADD_FAVORITE_URL = "account/{account_id}/favorite?"

const val ACCOUNT_STATES_URL = "tv/{$SHOW_ID_PATH}/account_states?"

