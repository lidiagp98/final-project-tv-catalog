package com.applaudotrainee.tvcatalog.data.tvshow.model

import com.google.gson.annotations.SerializedName

data class FavoriteBodyData(
    @field:SerializedName("media_type") val mediaType: String = "tv",
    @field:SerializedName("media_id") val mediaId: Int,
    @field:SerializedName("favorite") var favorite: Boolean
)