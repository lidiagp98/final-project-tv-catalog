package com.applaudotrainee.tvcatalog.data.tvshow.dao

import androidx.paging.DataSource
import androidx.room.*
import com.applaudotrainee.tvcatalog.data.tvshow.model.TvShowData
import com.applaudotrainee.tvcatalog.data.tvshow.relations.ShowWithAddonData

@Dao
interface TvShowDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertTvShows(tvShowList: List<TvShowData>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertTvShowDetails(tvShow: TvShowData)

    @Transaction
    @Query("SELECT * FROM TvShow WHERE showId =:showId")
    suspend fun getShowDetailsWithAddons(showId: Int): ShowWithAddonData

    @Query("DELETE FROM TvShow")
    suspend fun deleteAllTvShows()

    @Transaction
    suspend fun deleteAndInsertTvShowsDb(tvShowList: List<TvShowData>) {
        deleteAllTvShows()
        insertTvShows(tvShowList)
    }

    @Query("SELECT * FROM TvShow ORDER BY `order`")
    fun getAllTvShowsFromDb(): DataSource.Factory<Int, TvShowData>

    @Query("SELECT `order` FROM TvShow WHERE showId= :showId")
    suspend fun getOrderOfTvShow(showId: Int): Int

    @Query("SELECT * FROM TvShow WHERE favorite = :favorite")
    fun getFavoritesInDb(favorite: Boolean = true): DataSource.Factory<Int, TvShowData>

    @Query("SELECT favorite FROM TvShow WHERE showId= :showId")
    suspend fun getFavoriteState(showId: Int): Boolean?

}