package com.applaudotrainee.tvcatalog.data.session.model

import com.applaudotrainee.tvcatalog.data.util.DomainMapper
import com.applaudotrainee.tvcatalog.domain.session.model.SessionBody
import com.google.gson.annotations.SerializedName

data class SessionBodyData(
    @field:SerializedName("request_token") val access_token: String
) : DomainMapper<SessionBody> {
    override fun mapToDomainModel() = SessionBody(access_token)
}