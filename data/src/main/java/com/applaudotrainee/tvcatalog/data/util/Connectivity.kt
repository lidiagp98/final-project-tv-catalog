package com.applaudotrainee.tvcatalog.data.util

import androidx.lifecycle.MutableLiveData

interface Connectivity {
  var isNetworkConnected: MutableLiveData<Boolean>
  fun startNetworkCallback()
}