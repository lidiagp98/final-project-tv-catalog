package com.applaudotrainee.tvcatalog.data.util

import android.content.Context
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkRequest
import android.util.Log
import androidx.lifecycle.MutableLiveData
import kotlin.properties.Delegates

class ConnectivityImpl(private val context: Context) : Connectivity {

    override var isNetworkConnected = MutableLiveData(false)

    override fun startNetworkCallback() {
        val cm: ConnectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val builder: NetworkRequest.Builder = NetworkRequest.Builder()

        cm.registerNetworkCallback(
            builder.build(),
            object : ConnectivityManager.NetworkCallback() {

                override fun onAvailable(network: Network) {
                    isNetworkConnected.postValue(true)
                }

                override fun onLost(network: Network) {
                    isNetworkConnected.postValue(false)
                }
            })
    }
}