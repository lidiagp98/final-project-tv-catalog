package com.applaudotrainee.tvcatalog.data.tvshow.model

import com.applaudotrainee.tvcatalog.data.util.DomainMapper
import com.applaudotrainee.tvcatalog.domain.tvshow.model.EpisodeResponse
import com.google.gson.annotations.SerializedName

data class EpisodeResponseData(
    @field:SerializedName("season_number") val seasonNumber: Int,
    @field:SerializedName("id") val seasonId: Int,
    @field:SerializedName("episodes") val episodes: List<EpisodeData>
) : DomainMapper<EpisodeResponse> {
    override fun mapToDomainModel() =
        EpisodeResponse(seasonNumber, seasonId, episodes?.map { it.mapToDomainModel() })
}
