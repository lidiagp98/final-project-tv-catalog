package com.applaudotrainee.tvcatalog.data.session.repository

import com.applaudotrainee.tvcatalog.data.network.MovieDbApi
import com.applaudotrainee.tvcatalog.data.session.dao.SessionDao
import com.applaudotrainee.tvcatalog.data.session.model.SessionBodyData
import com.applaudotrainee.tvcatalog.data.util.Connectivity
import com.applaudotrainee.tvcatalog.domain.session.model.SessionResponse
import com.applaudotrainee.tvcatalog.domain.session.model.TokenResponse
import com.applaudotrainee.tvcatalog.domain.session.repository.SessionRepository
import org.koin.java.KoinJavaComponent.inject
import java.net.UnknownHostException

class SessionRepositoryIml(private val movieAPI: MovieDbApi, private val sessionDao: SessionDao) :
    SessionRepository {

    private val connectivity: Connectivity by inject(Connectivity::class.java)

    override suspend fun getRequestToken(): TokenResponse? {
        return if (connectivity.isNetworkConnected.value == true) {
            try {
                movieAPI.getTokenRequest().mapToDomainModel()
            } catch (e: UnknownHostException) {
                null
            }
        } else null
    }

    override suspend fun getSessionId(accessToken: String): SessionResponse? {
        return if (connectivity.isNetworkConnected.value == true) {
            val sessionBody = SessionBodyData(accessToken)
            try {
                val sessionResponseData = movieAPI.getSessionId(sessionBody)
                sessionDao.deleteSessionId()
                sessionDao.insertSessionId(sessionResponseData)
                sessionResponseData.mapToDomainModel()
            } catch (e: UnknownHostException) {
                null
            }
        } else null
    }

    override suspend fun getSessionIdFromDatabase(): SessionResponse? =
        sessionDao.getSessionId()?.mapToDomainModel()

    override suspend fun deleteSessionId() = sessionDao.deleteSessionId()
}