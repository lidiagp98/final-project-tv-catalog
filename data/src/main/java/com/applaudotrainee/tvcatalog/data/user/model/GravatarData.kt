package com.applaudotrainee.tvcatalog.data.user.model

import com.applaudotrainee.tvcatalog.data.util.DomainMapper
import com.applaudotrainee.tvcatalog.domain.user.model.Gravatar
import com.google.gson.annotations.SerializedName

data class GravatarData(

    @field:SerializedName("hash") val hash: String

) : DomainMapper<Gravatar> {
    override fun mapToDomainModel() = Gravatar(hash)
}