package com.applaudotrainee.tvcatalog.data.tvshow.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.applaudotrainee.tvcatalog.data.tvshow.model.CreatorData
import kotlinx.coroutines.flow.Flow

@Dao
interface CreatorDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertCreators(creator: List<CreatorData>)

    @Query("SELECT * FROM Creator WHERE creatorShowId = :creatorShowId")
    fun getCreatorsList(creatorShowId: Int): Flow<List<CreatorData>>
}