package com.applaudotrainee.tvcatalog.data.session.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.applaudotrainee.tvcatalog.data.util.DomainMapper
import com.applaudotrainee.tvcatalog.domain.session.model.SessionResponse
import com.google.gson.annotations.SerializedName

@Entity(tableName = "Session")
data class SessionResponseData(
    @field:SerializedName("success") val success: Boolean,
    @field:SerializedName("session_id") @PrimaryKey val sessionId: String
) : DomainMapper<SessionResponse> {
    override fun mapToDomainModel() = SessionResponse(success, sessionId)
}
