package com.applaudotrainee.tvcatalog.data.session.model

import com.applaudotrainee.tvcatalog.data.util.DomainMapper
import com.applaudotrainee.tvcatalog.domain.session.model.TokenResponse
import com.google.gson.annotations.SerializedName

data class TokenResponseData(
    @field:SerializedName("success") val success: Boolean,
    @field:SerializedName("request_token") val requestToken: String,
    @field:SerializedName("expires_at") val expiresAt: String
) : DomainMapper<TokenResponse> {
    override fun mapToDomainModel() = TokenResponse(success, requestToken, expiresAt)
}
