package com.applaudotrainee.tvcatalog.data.tvshow.model

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import com.applaudotrainee.tvcatalog.data.util.DomainMapper
import com.applaudotrainee.tvcatalog.domain.tvshow.model.Season
import com.google.gson.annotations.SerializedName

@Entity(
    tableName = "Season", foreignKeys = [ForeignKey(
        entity = TvShowData::class,
        parentColumns = ["showId"],
        childColumns = ["seasonShowId"],
        onDelete = ForeignKey.CASCADE
    )]
)
data class SeasonData(
    @field:SerializedName("id")
    @PrimaryKey
    val seasonId: Int,

    var seasonShowId: Int,

    @field:SerializedName("name")
    val name: String,

    @field:SerializedName("season_number")
    val seasonNumber: Int,

    @field:SerializedName("poster_path") val posterPath: String?

) : DomainMapper<Season> {
    override fun mapToDomainModel() = Season(seasonId, name, seasonNumber, posterPath)
}