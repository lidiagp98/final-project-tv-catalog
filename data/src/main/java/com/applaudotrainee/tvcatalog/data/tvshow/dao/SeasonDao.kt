package com.applaudotrainee.tvcatalog.data.tvshow.dao

import androidx.room.*
import com.applaudotrainee.tvcatalog.data.tvshow.model.SeasonData
import com.applaudotrainee.tvcatalog.data.tvshow.relations.SeasonWithEpisodeData
import kotlinx.coroutines.flow.Flow

@Dao
interface SeasonDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertSeasons(seasons: List<SeasonData>)

    @Transaction
    @Query("SELECT * FROM Season WHERE seasonShowId = :seasonShowId")
    suspend fun getSeasonsList(seasonShowId: Int): List<SeasonData>

    @Query("SELECT * FROM Season WHERE seasonId IN (:seasonsId) ORDER BY seasonNumber ASC")
    fun getSeasonWithEpisodes(seasonsId: List<Int>): Flow<List<SeasonWithEpisodeData>>
}