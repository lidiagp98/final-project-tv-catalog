package com.applaudotrainee.tvcatalog.data.user.repository

import androidx.lifecycle.LiveData
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.applaudotrainee.tvcatalog.data.network.MovieDbApi
import com.applaudotrainee.tvcatalog.data.session.dao.SessionDao
import com.applaudotrainee.tvcatalog.data.tvshow.dao.TvShowDao
import com.applaudotrainee.tvcatalog.data.user.FavoritesBoundaryCallback
import com.applaudotrainee.tvcatalog.domain.tvshow.model.TvShow
import com.applaudotrainee.tvcatalog.domain.user.repository.FavoriteListRepository

class FavoriteListRepositoryImpl(
    private val movieDbApi: MovieDbApi,
    private val sessionDao: SessionDao,
    private val showDao: TvShowDao
) : FavoriteListRepository<LiveData<PagedList<TvShow>>> {
    override suspend fun getFavoritesList(): LiveData<PagedList<TvShow>> {
        val config = PagedList.Config.Builder()
            .setPageSize(PAGE_SIZE)
            .setInitialLoadSizeHint(PAGE_SIZE)
            .setPrefetchDistance(PREFETCH_DISTANCE)
            .setEnablePlaceholders(false)
            .build()

        return LivePagedListBuilder(showDao.getFavoritesInDb().mapByPage { list ->
            list.map { it.mapToDomainModel() }
        }, config).setBoundaryCallback(FavoritesBoundaryCallback(movieDbApi, sessionDao, showDao)).build()
    }

    companion object {
        private const val PAGE_SIZE = 20
        private const val PREFETCH_DISTANCE = 10
    }
}
