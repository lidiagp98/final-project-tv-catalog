package com.applaudotrainee.tvcatalog.data.tvshow.repository

import com.applaudotrainee.tvcatalog.data.network.MovieDbApi
import com.applaudotrainee.tvcatalog.data.session.dao.SessionDao
import com.applaudotrainee.tvcatalog.data.tvshow.model.FavoriteBodyData
import com.applaudotrainee.tvcatalog.data.util.Connectivity
import com.applaudotrainee.tvcatalog.domain.tvshow.model.AccountStateResponse
import com.applaudotrainee.tvcatalog.domain.tvshow.model.FavoriteResponse
import com.applaudotrainee.tvcatalog.domain.tvshow.repository.FavoriteRepository
import org.koin.java.KoinJavaComponent

class FavoriteRepositoryImpl(
    private val movieDbApi: MovieDbApi,
    private val sessionDao: SessionDao
) : FavoriteRepository {

    private val connectivity: Connectivity by KoinJavaComponent.inject(Connectivity::class.java)

    override suspend fun addRemoveFavorite(showId: Int): FavoriteResponse? {
        val sessionId = sessionDao.getSessionIdFromDb().sessionId
        return if (connectivity.isNetworkConnected.value == true) {
            val showState = movieDbApi.getAccountStates(showId = showId, session_id = sessionId)
            val favoriteBody = FavoriteBodyData(favorite = false, mediaId = showId)
            if (showState.favorite) {
                movieDbApi.addFavorite(favoriteBody, session_id = sessionId).mapToDomainModel()
            } else {
                favoriteBody.favorite = true
                movieDbApi.addFavorite(favoriteBody, session_id = sessionId).mapToDomainModel()
            }
        } else null
    }

    override suspend fun getAccountState(showId: Int): AccountStateResponse? {
        return if(connectivity.isNetworkConnected.value == true){
            val sessionId = sessionDao.getSessionIdFromDb().sessionId
            movieDbApi.getAccountStates(showId,session_id = sessionId).mapToDomainModel()
        } else null
    }
}