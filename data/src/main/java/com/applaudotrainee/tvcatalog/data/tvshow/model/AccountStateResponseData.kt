package com.applaudotrainee.tvcatalog.data.tvshow.model

import com.applaudotrainee.tvcatalog.data.util.DomainMapper
import com.applaudotrainee.tvcatalog.domain.tvshow.model.AccountStateResponse
import com.google.gson.annotations.SerializedName

data class AccountStateResponseData(
    @field:SerializedName("id") val id: Int,
    @field:SerializedName("favorite") val favorite: Boolean
) : DomainMapper<AccountStateResponse> {
    override fun mapToDomainModel() = AccountStateResponse(id, favorite)
}
