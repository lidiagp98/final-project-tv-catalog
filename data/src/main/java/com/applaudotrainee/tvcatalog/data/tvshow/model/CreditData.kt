package com.applaudotrainee.tvcatalog.data.tvshow.model

import com.applaudotrainee.tvcatalog.data.util.DomainMapper
import com.applaudotrainee.tvcatalog.domain.tvshow.model.Credit
import com.google.gson.annotations.SerializedName

data class CreditData(
    @field:SerializedName("cast") val cast: List<CastData>?
) : DomainMapper<Credit> {
    override fun mapToDomainModel() = Credit(cast?.map { it.mapToDomainModel() })
}
