package com.applaudotrainee.tvcatalog.data

import com.applaudotrainee.tvcatalog.data.di.repositoryModule
import com.applaudotrainee.tvcatalog.data.network.MovieDbApi
import com.applaudotrainee.tvcatalog.data.session.dao.SessionDao
import com.applaudotrainee.tvcatalog.data.user.dao.UserDao
import com.applaudotrainee.tvcatalog.data.user.repository.UserRepositoryImpl
import com.applaudotrainee.tvcatalog.data.util.Connectivity
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.koin.core.context.loadKoinModules
import org.koin.core.context.startKoin
import org.koin.test.KoinTest
import org.koin.test.inject
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock

class UserRepositoryImplTest() {

    /* TODO
    private val movieDbApiMock = mock(MovieDbApi::class.java)
    private val sessionDaoMock = mock(SessionDao::class.java)
    private val userDaoMock = mock(UserDao::class.java)
    private val connectivity: Connectivity by inject()
    private val userRepository = UserRepositoryImpl(movieDbApiMock, sessionDaoMock, userDaoMock)

    @Before
    fun mockFunctionality() {
        startKoin {
            loadKoinModules(repositoryModule)
        }

        runBlocking {
            `when`(connectivity.isNetworkConnected.value).then { true }
            `when`(sessionDaoMock.getSessionId()).thenReturn(sessionResponse)
            `when`(userDaoMock.getUser()).thenReturn(userData)
            `when`(userDaoMock.insertUser(userData)).then { userDb.add(userData) }
        }

    }

    @Test
    fun getUserProfile_successfully_thenReturnUser() {
        runBlocking {
            val response = userRepository.getUserProfile()
            assertTrue(userDb.contains(userData))
            assertEquals(user, response)
        }
    }*/
}