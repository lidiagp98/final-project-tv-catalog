package com.applaudotrainee.tvcatalog.data

import com.applaudotrainee.tvcatalog.data.session.model.SessionResponseData
import com.applaudotrainee.tvcatalog.data.user.model.AvatarData
import com.applaudotrainee.tvcatalog.data.user.model.GravatarData
import com.applaudotrainee.tvcatalog.data.user.model.TmdbData
import com.applaudotrainee.tvcatalog.data.user.model.UserData

val sessionResponse = SessionResponseData(true, "eeeppp123456")
val gravatar = GravatarData("205e460b479e2e5b48aec07710c08d50")
val tmdb = TmdbData("/avatarPath")
val avatar = AvatarData(gravatar, tmdb)
const val USER_ID = 1
const val USERNAME = "user1"
val userData = UserData(avatar, USER_ID, USERNAME, USERNAME)
val user = userData.mapToDomainModel()
val userDb = mutableListOf<UserData>()