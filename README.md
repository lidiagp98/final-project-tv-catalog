# TvCatalog - Applaudo Trainee Program Final Proyect 2021
A display of tv shows using The Movie Database Api

* To add an api key, add to the proyect a file with the name "apikey.properties" and write your api key in the following format:
MOVIEDB_API_KEY = "your_api_key"

* The icons used in the app belong to Freepick/Flaticon https://www.freepik.com , https://www.flaticon.com
